const V1 = 'v1';
const V1_1 = 'v1.1';

module.exports.V1 = V1;
module.exports.V1_1 = V1_1;

module.exports.normaliseVersion = (version) => {
    if (version === V1_1)
        return V1_1;

    return V1;
};