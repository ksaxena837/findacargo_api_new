/**
 * Created by jeton on 7/11/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DeliverySettingsSchema = new Schema({
    home_delivery: {
        type: Boolean,
        required: [true, 'home_delivery is required']
    },
    store_pickup: {
        type: Boolean,
        required: [true, 'store_pickup is required']
    },
    week_days_available: {
        type: Array,
        required: [true, 'week_days_available is required']
    },
    zip: [
        {
            range_from: {
                type: String,
                // required: [true, 'range_from is required']
            },
            range_to: {
                type: String
                // required: [true, 'range_to is required']
            },
            price: {
                value: Number,
                currency: String
            }
        }
    ],
    delivery_windows: [
        {
            zip_from: String,
            zip_to: String,
            time_from: String,
            time_to: String
        }
    ],
    pickup_deadline: {
        type: String,
    },
    pickup_deadline_to: {
        type: String,
    },
    delivery_window_start: {
        type: String,
    },
});


var deliverySettings = mongoose.model('deliverySettings', DeliverySettingsSchema);
module.exports = deliverySettings;