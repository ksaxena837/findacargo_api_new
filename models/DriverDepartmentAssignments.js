var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DriverDepartmentAssignmentsSchema = new Schema({
    driver: {
        type: Schema.ObjectId,
        ref: 'accounts'
    },
    company: {
        type: Schema.ObjectId,
        ref: 'departments'
    },
    department: {
        type: String
    }
});


var accounts = mongoose.model('DriverDepartmentAssignments', DriverDepartmentAssignmentsSchema);
module.exports = accounts;