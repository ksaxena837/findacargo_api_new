let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let EventsHistory = new Schema({
    assigned_to: {
        type: Schema.ObjectId,
        required: true
    },
    assigned_to_type: {
        type: String,
        required: true
    },
    event: {
        type: String,
        required: true
    },
    event_data: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('events', EventsHistory);