let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let DeliveryTracking = new Schema({
    delivery_id: {
        type: Schema.ObjectId,
        ref: 'scheduleddeliveries'
    },
    latitude: Number,
    longitude: Number,
    created_at: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model('tracking', DeliveryTracking);