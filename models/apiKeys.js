/**
 * Created by jeton on 7/11/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ApiKeysSchema = new Schema({
    clientKey: String,
    clientName: String
});

var apiKeys = mongoose.model('apiKeys', ApiKeysSchema);
module.exports = apiKeys;