/**
 * Created by jeton on 7/11/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NotificationsSettingsSchema = new Schema({
    client_id: {
        type:  Schema.Types.ObjectId,
        required: [true, 'client_id is required']
    },
    sms_enabled: {
        type: Boolean,
        required: [true, 'sms is required']
    },
    email_enabled: {
        type: Boolean,
        required: [true, 'email_enabled is required']
    },
    phone_number: {
        type: String,
        required: [true, 'phone_number is required']
    },
    email: {
        type: String,
        required: [true, 'email is required']
    }
}
);


var notificationsSettings = mongoose.model('notificationsSettings', NotificationsSettingsSchema);
module.exports = notificationsSettings;