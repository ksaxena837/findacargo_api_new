var express = require('express');
var buyerDepartments = require("../lib/departments/buyerDepartments");
var Controller = require("../lib/framework/controller");
var db = require('../lib/framework/db-connector');
var mongoose = require('mongoose');

var router = express.Router();

var BuyerDepartmentController = new Controller({
    get: buyerDepartments.get,
    create: buyerDepartments.create,
    update: buyerDepartments.update,
    delete: buyerDepartments.delete
});

function find(collec, query, callback) {
    mongoose.connection.db.collection(collec, function(err, collection) {
        collection.find(query).toArray(callback);
    });
}

function findDepartments(collec, query, callback) {
    mongoose.connection.db.collection(collec, function(err, collection) {
        collection.find(query).toArray(callback);
    });
}

function createDepartments(collec, doc, callback) {
    mongoose.connection.db.collection(collec, function(err, collection) {
        collection.insert(doc, callback);
    });
}

function updateDepartments(collec, doc, callback) {
    mongoose.connection.db.collection(collec, function(err, collection) {
        collection.update({companyId: doc.companyId}, doc, callback);
    });
}

function deleteDepartments(collec, doc, callback) {
    mongoose.connection.db.collection(collec, function(err, collection) {
        collection.remove({companyId: doc.companyId}, callback);
    });
}

router.get('/get',function(req, res) {
    find('accounts', { apiKey: req.headers.token }, function(err, docs) {
        var User = docs[0]._id;
        if (err || docs.length < 1) {
            res.status(403).send({
                'success': false,
                'message': 'Invalid API Key.'
            });
        } else {
        	// check if the doc exists in departments
        	var account_id = docs[0]._id;
        	findDepartments('departments', { companyId: account_id }, function(err, dep_docs) {
		        if (dep_docs.length == 1) {
		            if (err){
		            	res.status(403).send("error");
		            }
		            else {
		            	delete dep_docs[0]._id;
		            	res.status(200).send(dep_docs[0]);
		            }
		        }
		        else {
		        	res.status(404).send("Departments not found");
		        }
        	})
        }
    })
});

router.post('/create', function(req, res) {
    var reqBody = req.body;
    find('accounts', { apiKey: req.headers.token }, function(err, docs) {
        reqBody["companyId"] = docs[0]._id;
		reqBody["companyName"] = docs[0].name;
        var User = docs[0]._id;
        if (err || docs.length < 1) {
            res.status(403).send({
                'success': false,
                'message': 'Invalid API Key.'
            });
        } else {
        	// check if the doc exists in departments
        	var account_id = docs[0]._id;
        	findDepartments('departments', { companyId: account_id }, function(err, dep_docs) {
		        if (err || dep_docs.length < 1) {
		            if (err){
		            	res.status(403).send("error");
		            }
		            else {
		            	createDepartments('departments', reqBody, function(err, resp) {
		            		if(resp.result && resp.result.ok != 1) {
		            			res.status(403).send("error");			
		            		}
		            		else {
		            			res.status(201).send(reqBody);
		            		}
		            	});
		            	
		            }
		        }
		        else {
		        	delete dep_docs[0]._id;
		        	res.status(200).send(dep_docs[0]);
		        }
        	})
        }
    })
});

router.put('/update', function(req, res) {
    var reqBody = req.body;
    find('accounts', { apiKey: req.headers.token }, function(err, docs) {
        reqBody["companyId"] = docs[0]._id;
        reqBody["companyName"] = docs[0].name;
        var User = docs[0]._id;
        if (err || docs.length < 1) {
            res.status(403).send({
                'success': false,
                'message': 'Invalid API Key.'
            });
        } else {
        	// check if the doc exists in departments
        	var account_id = docs[0]._id;
        	findDepartments('departments', { companyId: account_id }, function(err, dep_docs) {
		        if (dep_docs.length == 1) {
		            if (err){
		            	res.status(403).send("error");
		            }
		            else {
		            	updateDepartments('departments', reqBody, function(err, resp) {
		            		if(resp.result && resp.result.ok != 1) {
		            			res.status(403).send("error");			
		            		}
		            		else {
		            			res.status(200).send(reqBody);
		            		}
		            	});
		            	
		            }
		        }
		        else {
		        	delete dep_docs[0]._id;
		        	res.status(404).send("Departments Not found for this Buyer");
		        }
        	})
        }
    })
});

router.delete('/delete',function(req, res) {
    find('accounts', { apiKey: req.headers.token }, function(err, docs) {
        var User = docs[0]._id;
        if (err || docs.length < 1) {
            res.status(403).send({
                'success': false,
                'message': 'Invalid API Key.'
            });
        } else {
        	// check if the doc exists in departments
        	var account_id = docs[0]._id;
        	deleteDepartments('departments', { companyId: account_id }, function(err, resp) {
		        if (err) {
		        	res.status(403).send("error");
		        }
		        else {
		        	res.status(200).send("Successfully deleted!");
		        }
        	})
        }
    })
});

module.exports = router;
