var express = require("express");
var Controller = require("../lib/framework/controller");
var starter = require("../lib/scheduled").Starter;

var router = express.Router();

var StarterController = new Controller({
    start: starter.fromScheduled
});

router.post('/:scheduledId', StarterController.start);

module.exports = router;
