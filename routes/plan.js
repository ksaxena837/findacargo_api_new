let express = require("express");
let Controller = require("../lib/framework/controller");
let router = express.Router();
 
let ScheduleController = new Controller({
    get: require('../lib/scheduled/get'),
    create: require('../lib/scheduled/create'),
    statusUpdate: require('../lib/scheduled/statusUpdate'),
    search: require('../lib/scheduled/search'),
    print: require("../lib/scheduled/print"),
    eventsHistory: require("../lib/scheduled/events"),
    locationUpdate: require("../lib/scheduled/locationUpdate"),
    locationGet: require("../lib/scheduled/currentLocation")
}, {
    get: {
        auth: false
    },
    create: {
        schema: 'validator/schema/scheduled-delivery',
        auth: false
    },
    search: {
        schema: 'validator/schema/search'
    },
    print: {
        auth: false
    },
    eventsHistory: {
        auth: false
    },
    locationUpdate: {
        auth: false,
        schema: 'validator/schema/location-update'
    },
    locationGet: {
        auth: false
    }
});

const ROUTE_BASE_VERSIONING = `/:version/scheduled`;
const ROUTE_BASE_STATIC = '/v1/scheduled';

router.get(`${ROUTE_BASE_VERSIONING}/:id`, ScheduleController.get);
router.get(`${ROUTE_BASE_STATIC}/:id/location`, ScheduleController.locationGet);
router.post(`${ROUTE_BASE_STATIC}/:id/location`, ScheduleController.locationUpdate);
router.get(`${ROUTE_BASE_STATIC}/print/:id`, ScheduleController.print);
router.post(`${ROUTE_BASE_VERSIONING}/create`, ScheduleController.create);
router.put(`${ROUTE_BASE_STATIC}/:scheduleId/status/:status`, ScheduleController.statusUpdate);
router.get(`${ROUTE_BASE_STATIC}/:scheduleId/events`, ScheduleController.eventsHistory);
router.post(`${ROUTE_BASE_STATIC}/search`, ScheduleController.search);

module.exports = router;