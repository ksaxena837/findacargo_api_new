var express = require('express');
var Controller = require("../lib/framework/controller");
var setDeliveryStatus = require("../lib/deliveryStatus/setDeliveryStatus");

var deliveryController = new Controller({
    setCargoStatus: setDeliveryStatus.setStatus
},{
    deliveries: {
        schema: "validator/schema/mark-cargo"
    }
});


var router = express.Router();

router.post('/:status/:id', deliveryController.setCargoStatus);

module.exports = router;
