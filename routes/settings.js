var express = require("express");
var Response = require('../services/Response');
var DeliverySettings = require('../models/deliverysettings');
var mongoose = require('mongoose');

var router = express.Router();	

router.post('/config', function(req, res) {
    var data = req.body;
    var clientKey = req.header('x-api-key');
    mongoose.connection.db.collection('accounts').find({apiKey: clientKey}, function (err, response) {
        response.toArray(function (err, account) {
            DeliverySettings.update({clientId: account._id}, data, {upsert: true, setDefaultsOnInsert: true})
                .then(function (created) {
                    Response.created(res, 'Delivery Settings Created.', data);
                })
                .catch(function (err) {
                    if(err.name == 'ValidationError'){
                        var required = Response.mongooseValidation(err.errors);
                        Response.malformed(res, required.join(', '));
                    }
                })
        })
    })
});

router.get('/config', function (req, res) {
    var clientKey = req.header('x-api-key');
    mongoose.connection.db.collection('accounts').find({apiKey: clientKey}, function (err, response) {
        response.toArray(function (err, account) {
            DeliverySettings.findOne({clientId: account._id})
                .then(function (settings) {
                    if(settings){
                        Response.success(res, 'Delivery Settings Created.', settings);
                    }else{
                        Response.noData(res, 'Delivery Settings not found.')
                    }
                });
        })
    });
});



module.exports = router;
