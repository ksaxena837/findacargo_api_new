String.prototype.toObjectId = function() {
    var ObjectId = (require('mongoose').Types.ObjectId);
    return new ObjectId(this.toString());
};

Date.prototype.isValid = function () {
    return this.getTime() === this.getTime();
};

Date.prototype.getWeek = function() {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
}

global.ensureObjectId = function(value) {
    return typeof value === 'string' ? value.toObjectId() : value;
}