let DeliveryTrackingModel = require('../models/deliveryTracking'),
    request = require('request');

const TRACKING_SERVER = 'http://track.nemlevering.dk/location/';

class DeliveryTrackingService {
    static addTrackingData(deliveryId, location) {
        DeliveryTrackingService.notifyTrackingServer(deliveryId, location);
        return DeliveryTrackingModel.create({
            delivery_id: deliveryId,
            latitude: location.lat,
            longitude: location.lon
        });
    }

    static getTrackingData(deliveryId) {
        return DeliveryTrackingModel.find(deliveryId);
    }

    static notifyTrackingServer(delivery, location) {
        return request({
            method: 'post',
            uri: TRACKING_SERVER + delivery,
            json: true,
            body: location
        })
    }
}

module.exports = DeliveryTrackingService;