let EventsRepository = require('../lib/event-history/repository');

const SCHEDULED_DELIVERY = 'SCHEDULED_DELIVERY';
const STATUS_UPDATE = 'STATUS_UPDATE'
const DATE_CHANGED = 'DATE_CHANGED'

class EventHistoryService {

    static createEvent(assigned_to, assigned_to_type, event, event_data) {
        assigned_to = ensureObjectId(assigned_to);
        return EventsRepository.create(assigned_to, assigned_to_type, event, event_data);
    }

    static getEvent(type, id) {
        id = ensureObjectId(id);
        return EventsRepository.findForIdOfType(id, type);
    }

    static statusUpdatedScheduledDelivery(id, newStatus) {
        return this.createEvent(id, SCHEDULED_DELIVERY, STATUS_UPDATE, newStatus);
    }
    
    static dateChangedScheduledDelivery(id, event_data) {
        return this.createEvent(id, SCHEDULED_DELIVERY, DATE_CHANGED, event_data);
    }

    static getEventsForScheduledDelivery(id) {
        return this.getEvent(SCHEDULED_DELIVERY, id);
    }
}

module.exports = EventHistoryService;