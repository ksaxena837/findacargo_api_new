var User = {};
var UserAgent;
var IOSDevice = false;

function getUser() {
    return User;
}

function getUserAgent() {
    return UserAgent;
}

function setUser(account) {
    User = account;
    User.id = account._id;
}

function setUserAgent(ua) {
    UserAgent = ua;
}

function isIOS() {
    return IOSDevice;
}

function setIsIOS(value) {
    IOSDevice = value;
}

module.exports = {
    getUser: getUser,
    setUser: setUser,
    getUserAgent: getUserAgent,
    setUserAgent: setUserAgent,
    setIsIOS: setIsIOS,
    isIOS: isIOS
};
