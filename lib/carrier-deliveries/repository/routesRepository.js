let db = require('../../framework/db-connector'),
    RouteSchema = require('./models/route');

module.exports = {
    getRoutesForDate: function (accountId, startDate, endDate, locationId) {
        var query = [
            {
                "$match": {
                    "solution.routes.driverId": accountId.toString(),
                    "routeDate": {
                        $gte: startDate,
                        $lt: endDate
                    }
                }
            },
            {
                "$unwind": "$solution.routes"
            },
            {
                "$match":
                    {
                        "solution.routes.driverId": accountId.toString(),
                        "routeDate": {
                            $gte: startDate,
                            $lt: endDate
                        }
                    }
            },
        ];

        if (locationId) {
            query[0].activities = {location_id: locationId};
        }

        return RouteSchema.aggregate(query);
    }
};