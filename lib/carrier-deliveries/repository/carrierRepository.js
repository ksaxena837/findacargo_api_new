var mongodb = require("mongodb");
var mongoose = require("mongoose");
var db = require('../../framework/db-connector');
var moment = require("moment");

var Promise = require("bluebird");
var q = require("q");
var Account = mongoose.models.accounts;
var Cargo =  db.collection('cargo');
var Contacts = db.collection('contacts');

var Deliveries = db.collection('deliveries');
let RoutesRepository = require('./routesRepository'),
    ScheduledDeliveriesRepository = require('./scheduleDeliveriesRepository'),
    DriverDeliveryRepository = require('./driverDeliveryRepository');

var Repository = {
    getById: getById,
    getByEmail: getByEmail,
    getDeliveries:getDeliveries,
    getContact:getContact,
    getRoutesAndScheduledDeliveries: getRoutesAndScheduledDeliveries,
    getZoneDeliveries: getZoneDeliveries
}

function getRoutesAndScheduledDeliveries(date, accountId, locationId) {
    var start = moment(date, "MM-DD-YYYY").startOf('day');
    var end = moment(date, "MM-DD-YYYY").add(1, "days");

    let startDate = new Date(start.format('YYYY-MM-DD'));
    let endDate = new Date(end.format('YYYY-MM-DD'));

    return RoutesRepository.getRoutesForDate(accountId, startDate, endDate, locationId)
        .then(function(routes){
            var promises = routes.map(function(routeItem){
                return getScheduledDeliveries(date, routeItem);
            });

            return Promise.all(promises)
                .then(function(out){
                    var report = [];
                    for(var i=0; i<out.length; i++){
                        report = report.concat(out[i]);
                    }

                    return report;
                });
        });
}

function getScheduledDeliveries(date, routeItem) {
    var start = moment(date, "MM-DD-YYYY");
    var end = moment(date, "MM-DD-YYYY").add(1, "days");
    let route = routeItem.solution.routes;

    return new Promise(function(resolve, reject){
        let ids = route.activities.map(function(locality){
            return locality.location_id;
        });

        ScheduledDeliveriesRepository.scheduled.forRouteDate(start, end, routeItem, ids).toArray(function(error, response) {
            if(error){
                return reject(error);
            }

            var deliveries = ids.map(function(id){
                let scheduled = response.find(function(document){
                    return document.deliveryid == id;
                });

                if (scheduled) {
                    return {
                        from: scheduled
                    }
                }
            });

            resolve(deliveries.filter(Boolean));
        });
    })
    .then(function(deliveries){
        var promises = deliveries.map(function(item){
            return new Promise(function(resolve, reject){
                Deliveries.findOne({_id:new mongodb.ObjectId(item.from.delivery_id)}, function(error, response){
                    if(error){
                        resolve(item);
                    }

                    item.deliveryData = response;
                    resolve(item);
                });
            
            });
        });

        return Promise.all(promises);
    });
}

function getZoneDeliveries(carrierId, date) {
    return new Promise((resolve, reject) => {
        return DriverDeliveryRepository.getAssignedDeliveries(carrierId.toString()).toArray((error, deliveries) => {
            let start = moment(date, "MM-DD-YYYY");
            let end = moment(date, "MM-DD-YYYY").add(1, "days");

            let ids = deliveries.map((delivery) => {
                return delivery.delivery_id.toObjectId();
            });

            return ScheduledDeliveriesRepository.zone.forDate(start, end, ids).toArray((error, res) => {
                let result = res.map((delivery) => {
                    return { from : delivery };
                });
                resolve(result);
            });
        });
    });
}

function getDeliveries(userData, accountID)
{
    var deferred = q.defer();
    var regex = RegExp("/.*" + userData + ".*/")
    var query = {"userID":new mongodb.ObjectId(accountID),"deliveryDate":new RegExp('^' + userData)}

    Cargo.find(query).toArray(function(error, response){
        if(response){
            if(response.length==0){
                deferred.reject("No Deliveries");
            }

            deferred.resolve(response)
        }else{
            deferred.reject(error);
        }
    });

    return deferred.promise;
}

function getContact(deliveries)
{
    return new Promise(function(resolve, reject){
        if(deliveries=="undefined")
        {
            deliveries = "000000000000000000000000"
        }

        Contacts.findOne({_id: new mongodb.ObjectId(deliveries)}, function (err, res) {
            if (res) {
                resolve(res);
            } else {
                reject(err);
            }
        });
    });
}


function getById(id) {
    var deferred = q.defer();

    Account.findById(id, function(err, userAccount){
                     if(err) {
                     return deferred.reject(err);
                     }

                     deferred.resolve(userAccount);
                     });
    
    return deferred.promise;
}

function getByEmail(email) {
    var deferred = q.defer();
    var query = {
        email: email,
    };

    Account.findOne(query, function(err, account){
        if(err){
            return deferred.reject(err);
        }

        deferred.resolve(account);
    });

    return deferred.promise;
}

module.exports = Repository;
