let moment = require('moment'),
    ScheduledDeliveriesRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    PostalCodeRepository = require('../postal-codes/repository'),
    _ = require('lodash');

function DeliveriesForDate(date) {
    const forDate = moment(date, 'YYYY-MM-DD'),
          toDate = moment(date, 'YYYY-MM-DD').add(1, 'd');

    return new Promise((resolve, reject) => {
        ScheduledDeliveriesRepository.scheduled
            .forDate(forDate, toDate)
            .toArray((err, deliveries) => {
                if (err)
                    return reject(err);

                let result = deliveries.map(delivery => {
                    return PostalCodeRepository.getForZipCode(delivery.deliveryzip)
                        .then((postalObject) => {
                            if (!postalObject) return;

                            return {
                                delivery_id: delivery._id,
                                zone: `${postalObject.AREA}${postalObject.ZONE}`,
                                zip: delivery.deliveryzip,
                                department: delivery.department,
                                company_id: delivery.creator,
                                address: delivery.deliveryaddress
                            };
                        }).catch(err => {
                            console.error(err);
                        })

                });

                return Promise.all(result)
                    .then(data => {
                        return resolve(data);
                    })
            });
    })
}


module.exports = (date) => { return new DeliveriesForDate(date); };