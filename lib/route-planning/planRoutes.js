let DriverZoneAssignment = require('../../models/DriverZoneAssignment'),
    DriverDepartmentAssignments = require('../../models/DriverDepartmentAssignments'),
    _ = require('lodash'),
    RoutePlannerService = require('../../services/RoutePlannerService'),
    DepartmentRepository = require('../departments/repository/departmentRepository');

const TYPE_DISTRIBUTION_ZONE = 'Distribution';
const TYPE_DEDICATED = 'Dedicated Drivers';

function PlanRoutes(request, response) {
    return this.getDriversPerDelivery(request.body.deliveries)
        .then(this.groupByDriver)
        .then((list) => {
            return this.planRoutePerDriver(list)
                .then((data) => {
                    response.writeHead(200, {"Content-Type": "application/json"});
                    return response.end(JSON.stringify({
                        assigned: data.assigned || [],
                        notAssigned: data.withoutDriver || []
                    }));
                });
        })
        .catch(err => {
            console.log(err);
            response.writeHead(500, {"Content-Type": "application/json"});
            return response.end(JSON.stringify({status: 500, msg: err}));
        });
}

PlanRoutes.prototype.getDriversPerDelivery= function (list) {
    let updatedList = list.map(delivery => {
        if (!delivery) return;
        return DepartmentRepository.findDepartmentOfCompany(delivery.company_id, delivery.department)
            .then(department => {
                if (department.typeOfDelivery === TYPE_DISTRIBUTION_ZONE)
                    return this.findDriverForZone(delivery);

                return this.findDriverForDepartment(delivery);
            })
    });

    return Promise.all(updatedList)
};

PlanRoutes.prototype.findDriverForZone = function (delivery) {
    return DriverZoneAssignment.findOne({
        zone: delivery.zone
    }).then((result) => {
        if (!result) {
            delivery.haveDriver = false;
        }
        else {
            delivery.haveDriver = true;
            delivery.driver_id = result.driver;
        }
        return delivery;
    });
};

PlanRoutes.prototype.findDriverForDepartment = function (delivery) {
    return DepartmentRepository.findByCompanyId(delivery.company_id)
        .then(Company => {
            if (!Company) {
                delivery.haveDriver = false;
                return delivery
            }

            return DriverDepartmentAssignments.findOne({
                company: Company._id,
                department: delivery.department
            }).then(assignment => {
                if (!assignment)
                    delivery.haveDriver = false;
                else {
                    delivery.haveDriver = true;
                    delivery.driver_id = assignment.driver;
                }

                return delivery;
            })
        })
};

PlanRoutes.prototype.groupByDriver = function (list) {
    let newList = _.groupBy(list, 'haveDriver');
    return {
        haveDriver: _.groupBy(newList.true, 'driver_id'),
        withoutDriver: newList.false
    }
};

PlanRoutes.prototype.planRoutePerDriver = function (list) {
    return RoutePlannerService.planRoute(list);
};


module.exports = (req, res) => { return new PlanRoutes(req, res) };