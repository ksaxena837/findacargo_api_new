var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "CarrierRecoveriesADelivery",
    "description": "Validate carrier data to recover the delivery",
    "type": "object",
    "properties": {
        "vehicleId": {
            "description": "The carrier vehicle identifier to assign the delivery",
            "type": "string"
        },
    },
    "required": ["vehicleId"]
};

module.exports = Schema;
