var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "UserLogin",
    "description": "Validate user data to login",
    "type": "object",
    "properties": {
        "email": {
            "description": "The user email",
            "type": "string"
        },
        "password": {
            "description": "The user password",
            "type": "string"
        },
    },
    "required": ["email", "password"]
};

module.exports = Schema;
