var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "UserRating",
    "description": "Validate user preferred location",
    "type": "object",
    "properties": {
        "description": {
            "description": "Location description",
            "type": "string",
        },
        "latitude": {
            "description": "Location latitude coordinate",
            "type": "string"
        },
        "longitude": {
            "description": "Location longitude coordinate",
            "type": "string"
        },
    },
    "required": ["description", "latitude", "longitude"]
};

module.exports = Schema;
