var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "UserLogin",
    "description": "Validate user data to signup",
    "type": "object",
    "properties": {
        "email": {
            "description": "The user email",
            "type": "string"
        },
        "password": {
            "description": "The user password",
            "type": "string"
        },
        "carrier": {
            "description": "Flag that means the carrier role",
            "type":"string"
        },
        "buyer": {
            "description": "Flag that means the buyer role",
            "type":"string"
        }
    },
    "anyOf": [
        {"required": ["email", "password", "carrier"]},
        {"required": ["email", "password", "buyer"]},
        {"required": ["email", "password", "carrier", "buyer"]},
    ]
};

module.exports = Schema;
