var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "OnboardingVehicle",
    "description": "Validate vehicle data after signup",
    "type": "object",
    "properties": {
        "identifier": {
            "description": "Vehicle identifier",
            "type": "string"
        },
        "vehicleType": {
            "description": "Vehicle main type description",
            "type": "string"
        },
        "type": {
            "description": "Vehicle type id",
            "type": "string"
        },
        "allowCargo": {
            "description": "Allowed cargo description",
            "type": "string"
        },
    },
    "required": ["identifier", "vehicleType", "type"]
};

module.exports = Schema;
