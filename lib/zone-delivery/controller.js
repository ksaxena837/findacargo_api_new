let DriverDeliveryRepository = require('../carrier-deliveries/repository/driverDeliveryRepository'),
    ScheduledDeliveryRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    PostalCodeRepository = require('../postal-codes/repository'),
    AccountRepository = require('../user/repository/account.js'),
    DeliverySettingsRepository = require('../user/repository/deliverysettings.js'),
    Formatter = require('../scheduled/formatScheduled'),
    moment = require('moment'),
request = require('request'),    
ScheduledRepository = require('../scheduled/repository/scheduled'),
    EventHistoryService = require('../../services/EventHistoryService');

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

let ZoneDeliveryController = {
    getAllForDriver: function (id) {
        return new Promise((resolve, reject) => {
            DriverDeliveryRepository.getAssignedDeliveries(id.toObjectId())
                .toArray((err, listOfIds) => {
                    if (err) reject(err);

                    let ids = listOfIds.map((element) => {
                        return element.delivery_id;
                    });

                    ScheduledDeliveryRepository.zone.all(ids).toArray((err, deliveries) => {
                        if (err) reject(err);

                        let formattedDeliveries = deliveries.map(delivery => {
                            return Formatter({from: delivery});
                        });

                        resolve(formattedDeliveries);
                    });
                })
        });
    },

    findAssignment: function (deliveryId) {
        return new Promise((resolve, reject) => {
            DriverDeliveryRepository.findOne({delivery_id: deliveryId.toString()}).then((result) => {
                resolve(result);
            }).catch(err => {
                reject(err)
            });
        });
    },

    create: function (body) {
        return new Promise((resolve, reject) => {
            DriverDeliveryRepository.create({
                carrier_id: body.driver_id,
                delivery_id: body.delivery_id
            });

            let query = {};

            if (body.order)
                query.orderIndex = body.order;

            if (body.estimated_delivery_time)
                query.estimated_delivery_time = body.estimated_delivery_time;

            if (body.order && body.estimated_delivery_time)
                query.status = 2;

            if (query === {})
                return resolve();

            ScheduledDeliveryRepository.scheduled.update(body.delivery_id.toObjectId(), query);
            resolve();
        });
    },

    update: function (body) {
        return new Promise((resolve, reject) => {
            let query = {};

            if (body.order) {
                query.status = 2;
                query.orderIndex = body.order;
            }

            if (body.estimated_delivery_time)
                query.estimated_delivery_time = body.estimated_delivery_time;

            if (query === {})
                return resolve();

            ScheduledDeliveryRepository.scheduled.findOneAndUpdate(body.delivery_id.toObjectId(), query);
            DriverDeliveryRepository.findOneAndUpdate({
                delivery_id: body.delivery_id
            }, {
                carrier_id: body.driver_id
            });

            resolve();

        });
    },

    updateMany: function (body) {
        var assignments = body.filter(x => x.driver_id);

        return new Promise((resolve, reject) => {
            DriverDeliveryRepository.deleteMany(body.map(x => x.delivery_id))
                .then(function (data) {
                    if (data.result && data.result.ok) {
                        return assignments;
                    }
                }).then(function (data) {
                if (data) {
                    var newAssignments = data.map(x => {
                        return {
                            carrier_id: x.driver_id,
                            delivery_id: x.delivery_id
                        }
                    });

                    return DriverDeliveryRepository.createMany(newAssignments);
                }
            }).then(function (data) {
                if (data.result && data.result.ok) {
                    return assignments;
                }
            }).then(function (data) {
                if (data) {
                    var promises = [];
                    data.forEach(assignment => {
                        let query = {};

                        if (assignment.order)
                            query.orderIndex = assignment.order;

                        if (assignment.estimated_delivery_time)
                            query.estimated_delivery_time = assignment.estimated_delivery_time;

                        if (assignment.order && assignment.estimated_delivery_time)
                            query.status = 2;

                        if (query != {}) {
                            var promise = ScheduledDeliveryRepository.scheduled.findOneAndUpdate(assignment.delivery_id.toObjectId(), query);
                            promises.push(promise);
                        }
                    });

                    return Promise.all(promises);
                }
            }).then(function () {
                resolve();
            });
        });
    },

    updateStatus: function (status, deliveryIds) {
        let ids = deliveryIds.map(x => {
            return x.toObjectId();
        });

        deliveryIds.forEach((item) => {
            if (parseInt(status) === 3) {
                EventHistoryService.statusUpdatedScheduledDelivery(item, 3);
            }
        });

        return new Promise((resolve, reject) => {
            ScheduledDeliveryRepository.scheduled.updateStatus(ids, status).then(resolve());
	deliveryIds.forEach(deliveryId => {
		ScheduledRepository.getByIdFromString(deliveryId).then((delivery) => {
 	ScheduledRepository.updateDeliveryStatus(deliveryId, status)
            .then((updatedDelivery) => {
                updatedDelivery = updatedDelivery._doc;
                if (updatedDelivery.status == 2) {
                    let end_time = updatedDelivery.estimated_delivery_time;
                    let schedule = {
                    deliverydate: updatedDelivery.deliverydate,
                    deliverywindowend: end_time ? end_time.substring(end_time.indexOf("-") + 2) : '',
                    recipientemail: updatedDelivery.recipientemail,
                    recipientname: updatedDelivery.recipientname,
                    recipientphone: updatedDelivery.recipientphone['country_dial_code']+updatedDelivery.recipientphone['phone']
                           }
                     //Notification
                    request.post('https://notify.nemlevering.dk/api/order/create', {json: schedule}, function (err, resp, bd) {
                    if (err) console.log(err)
                    console.log(bd);
                     })
                    //end
                }
                else if (updatedDelivery.status == 3)
                {
                    let end_time = updatedDelivery.estimated_delivery_time;
                    let schedule = {
                    deliverywindowend: end_time ? end_time.substring(end_time.indexOf("-") + 2) : '',
                    recipientemail: updatedDelivery.recipientemail,
                    recipientname: updatedDelivery.recipientname,
                    recipientphone: updatedDelivery.recipientphone.country_dial_code+updatedDelivery.recipientphone.phone
                           }
                     //Notification
                    request.post('https://notify.nemlevering.dk/api/order/complete', {json: schedule}, function (err, resp, bd) {
                    if (err) console.log(err)
                    console.log(bd);
                     })
                    //end
                }
              
            });
    });


	});
        });
    },

    updateDate: function (date, deliveryIds) {
        let ids = deliveryIds.map(x => {
            return x.toObjectId();
        });

        let deliverydate = new Date(date);

        if (isNaN(deliverydate.getTime())) {
            return;
        }

        let weekNumber = deliverydate.getISOWeek().toString();
        let deliverydayofweek = days[deliverydate.getUTCDay()];

        return new Promise((resolve, reject) => {
            ScheduledDeliveryRepository.scheduled.updateDate(ids, deliverydate, weekNumber, deliverydayofweek).then(resolve());
        });
    },

    deleteMany: function (deliveryIds) {
        let ids = deliveryIds.map(x => {
            return x.toObjectId();
        });

        return new Promise((resolve, reject) => {
            ScheduledDeliveryRepository.scheduled.deleteMany(ids).then(resolve());
        });
    },

    delete: function (deliveryId) {
        return new Promise((resolve, reject) => {
            DriverDeliveryRepository.delete({
                delivery_id: deliveryId
            });
            resolve();
        });
    },

    forDate: function (date) {
        var data = [];
        return new Promise((resolve, reject) => {
            let startDate = moment.utc(date, "MM-DD-YYYY").startOf('day');
            let endDate = moment.utc(date, "MM-DD-YYYY").add(1, "days");

            ScheduledDeliveryRepository.scheduled.forDate(startDate, endDate)
                .toArray((err, deliveries) => {
                    if (err) reject(err);

                    resolve(deliveries);
                });
        }).then(deliveries => {
            data = deliveries;
            return AccountRepository.findAll({
                '_id': { $in: deliveries.map(x => x.creator) }
            }, { name: 1 });
        }).then(creators => {
            deliveries = data.map(element => {
                let creator = creators.find(x => x._id.equals(element.creator));
                element.creatorName = creator && creator.name ? creator.name : '';
                return element;
            });

            return Promise.all(deliveries.map(delivery => {
                return ZoneDeliveryController.appendDeliveryData(delivery);
            }));
        });
    },

    clientPickups: function (creator) {
        return ZoneDeliveryController.getPickups(null, creator);
    },

    pickups: function (date) {
        return ZoneDeliveryController.getPickups(date, null);
    },

    getPickups: function (date, creator) {
        let data = [];
        return new Promise((resolve, reject) => {
            // get data for the current year

            ScheduledDeliveryRepository.scheduled.pickups(creator, date)
                .toArray((err, data) => {
                    if (err) reject(err);
                    resolve(data);
                });
        }).then((res) => {
            data = res;
            return AccountRepository.findAll({
                '_id': {$in: data.map(item => item._id.creator)}
            }, {name: 1});
        }).then((creators) => {
            data = data.map(element => {
                let creatorObj = creators.find(item => item._id.equals(element._id.creator));
                return Object.assign({}, element._id, {
                    creator: {
                        id: element._id.creator,
                        name: creatorObj.name
                    },
                    deliveriesCount: element.deliveriesCount
                });
            });

            return data.filter(x => x.creator.name);
        }).then((res) => {
            data = res;
            return DeliverySettingsRepository.getByClientIds(data.map(x => x.creator.id));
        }).then((deliverySettings) => {
            if (deliverySettings.length <= 0) {
                return data;
            }

            return data.map(element => {
                let deliverySetting = deliverySettings.find(item => item._doc.clientId.equals(element.creator.id)) || null;

                if (!deliverySetting) {
                    return element;
                }

                element.pickup_deadline = deliverySetting.pickup_deadline;
                element.pickup_deadline_to = deliverySetting.pickup_deadline_to;
                element.delivery_window_start = deliverySetting.delivery_window_start;

                return element;
            });
        }).then(data => {
            // when pickup window is a later date than delivery window, assume pickup happens on the previous day
            data.map(x => {
                if (moment(x.pickup_deadline, 'HH:mm') > moment(x.delivery_window_start, 'HH:mm')) {
                    x.date.setDate(x.date.getDate() - 1);
                }
            });

            return data.sort(function (a, b) {
                return (a.creator.name).localeCompare((b.creator.name)) || (new Date(b.date)).getTime() - (new Date(a.date)).getTime();
            });
        });
    },

    appendDeliveryData: function (delivery) {
        return ZoneDeliveryController.assignZipCodeToDelivery(delivery)
            .then(delivery => {
                return ZoneDeliveryController.appendCarrierAssignmentToDelivery(delivery);
            });
    },

    assignZipCodeToDelivery: function (delivery) {
        if (!delivery.deliveryzip)
            return delivery;

        return PostalCodeRepository.getForZipCode(delivery.deliveryzip)
            .then((postalObject) => {
                if (postalObject) {
                    delivery.area = postalObject.AREA;
                    delivery.zone = postalObject.ZONE;
                }

                return delivery;
            });
    },

    appendCarrierAssignmentToDelivery: function (delivery) {
        return ZoneDeliveryController.findAssignment(delivery._id).then(assignment => {
            if (assignment && assignment.carrier_id)
                delivery.assigned_to = assignment.carrier_id;

            return delivery;
        });
    }
};

module.exports = ZoneDeliveryController;
