var vehicleRepository = require('./repository/vehicle');
var timeCalculator = require('../delivery').TimeCalculator;
var costCalculator = require('../delivery').CostCalculator;
var distanceCalculator = require('../delivery/calculator').GMapsDistance;
var vehicleFormatter = require('./formatter/vehicle');
var locationFactory = require('./factory/location');
var getUser = require('../auth/user').getUser;
var rateCardRepository = require("./repository/rate-card");
var Promise = require("bluebird");
var q = require('q');


var Inventary = {
    listAvailable: listAvailable,
    listVehicles: listVehicles,
    bidVehicle: bidVehicle,
    setVehicleLocation: setVehicleLocation,
    getTypeRateCard: getTypeRateCard,
    getAvailability: getAvailability,
    getVehicleLocation: getVehicleLocation
};

function listAvailable(pickUp, dropOff) {
    var pickUpLocation = locationFactory.createFromCommaSeparatedString(pickUp);
    var dropOffLocation = locationFactory.createFromCommaSeparatedString(dropOff);
    var filter = [ pickUpLocation.longitude, pickUpLocation.latitude ];
   
    return vehicleRepository.getTypesAvailable(filter)
        .then(function(typesList){
            if(!dropOffLocation){
                return addPriceAndTime(typesList, 0);
            }
            return distanceCalculator.calculate(pickUpLocation, dropOffLocation)
                    .then(function(distance){
                        return addPriceAndTime(typesList, distance);
                    });
        })
        .then(function(data) {
            var costCalculator = require('../delivery').CostCalculator;
            return vehicleFormatter.availability(data, pickUpLocation, dropOffLocation, costCalculator.distance);
        });

    function addPriceAndTime(typesList, distance) {
        var promises = [];
        for(var n in typesList) {
            promises.push(rateCardRepository.getForVehicles(typesList[n]["vehicles"]));
        }

        return Promise.all(promises).then(function(rateCardList){
            var costCalculator = require('../delivery').CostCalculator;
            var timeCalculator = require('../delivery').TimeCalculator;

            var defaultRateCard = costCalculator.getRateCard();
            for(var n in typesList) {
                costCalculator.setRateCard(defaultRateCard);
                var rateCard = costCalculator.getRateCard().getByType(typesList[n]["_id"].id);
                var base = rateCard.base;
                var perKm = rateCard.costPerKm;
                if(rateCardList[n]) {
                    base = rateCardList[n].basePrice;
                    perKm = rateCardList[n].pricePerKm;
                }
                typesList[n].price = costCalculator.estimate(typesList[n]["_id"].id, pickUpLocation, dropOffLocation, distance);
                typesList[n].timeToPickUp = timeCalculator.calculate(typesList[n].distance, typesList[n].vehicles.length > 0, rateCard.velocity);
                typesList[n].basePrice = base; 
                typesList[n].pricePerKm =  perKm;
            }
            return typesList;
        });

    }
}

function getAvailability(delivery) {
    return vehicleRepository.getTypesAvailable(delivery.pickUp.location, delivery.vehicleType)
                .then(function(typesList){
                    typeData = typesList.pop();

                    if(!typeData) {
                        return false;
                    }

                    return typeData["carriers"].length > 0;
                });
}

function getTypeRateCard(typeId, pickup){
    var filter = [parseFloat(pickup.longitude), parseFloat(pickup.latitude)];

    return vehicleRepository.getTypesAvailable(filter, typeId)
                .then(function(typesList){
                    typeData = typesList.pop();

                    if(!typeData) {
                        return false;
                    }

                    return rateCardRepository.getForCarriersAndType(typeData["carriers"], typeId);
                });
}

function bidVehicle(deliveryModel) {
    var blacklist = deliveryModel.carriers.map(function(item){
        return item.accountId;
    });
    return vehicleRepository
        .getLiveAvailable(deliveryModel.vehicleType, deliveryModel.pickUp.location, blacklist)
        .then(vehicleFormatter.bidVehicle);
}

function setVehicleLocation(vehicleId, coordinates) {
    var deferred = q.defer();
    var User = getUser();

    vehicleRepository
        .updateLocation(vehicleId, User.id, coordinates)
        .then(vehicleFormatter.updateStatus)
        .then(deferred.resolve, deferred.reject);

    return deferred.promise;
}

function getVehicleLocation(vehicleId){
    return vehicleRepository.getLocation(vehicleId);
}

function listVehicles() {
    var user = getUser();

    return vehicleRepository
                .findByCarrierId(user.id)
                .then(vehicleFormatter.list)
}

module.exports = Inventary;
