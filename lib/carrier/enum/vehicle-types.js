var mongoose = require("mongoose");
mongoose.Promise = require("bluebird");
var vehicleTypeSchema = require("../repository/models/vehicle-type");
var VehicleType = mongoose.models.VehicleType;

var VehicleTypes = [];
var ordered = [];

VehicleType.find({}).lean().sort({"base":"asc","typeId":"asc"}).exec()
    .then(function(data){
        for(var i in data){
            VehicleTypes[data[i].typeId] = data[i];
            ordered.push(data[i]);
        }

    });
/*
var VehicleTypes  = {
    1: {base: 90, costPerKm: 10, description: "Pickup", cargo: "25 kg max - 187m3"},
    2: {base: 90, costPerKm: 10, description: "Van", cargo: "25 kg max - 187m3"},
    3: {base: 150, costPerKm: 10, description: "Box Truck", cargo: "25 kg max - 187m3" },
    4: {base: 250, costPerKm: 10, description: "Flat Bed", cargo: "25 kg max - 187m3" },
    5: {base: 250, costPerKm: 10, description: "Tow Truck", cargo: "25 kg max - 187m3"},
    6: {base: 250, costPerKm: 10, description: "Concrete / Cement Mixture", cargo: "25 kg max - 187m3" },
    7: {base: 250, costPerKm: 10, description: "Mobile Crane", cargo: "25 kg max - 187m3" },
    8: {base: 250, costPerKm: 10, description: "Dump Truck", cargo: "25 kg max - 187m3" },
    9: {base: 250, costPerKm: 10, description: "Garbage Truck", cargo: "25 kg max - 187m3" },
    10: {base: 250, costPerKm: 10, description: "Log Carrier", cargo: "25 kg max - 187m3" },
    11: {base: 250, costPerKm: 10, description: "Refrigerated", cargo: "25 kg max - 187m3" },
    12: {base: 250, costPerKm: 10, description: "Tank Truck", cargo: "25 kg max - 187m3" },
    13: {base: 250, costPerKm: 10, description: "Long Haul / Tractor Truck", cargo: "25 kg max - 187m3" },
}
*/
function getType(id) {
    return VehicleTypes[parseInt(id)];
}

module.exports = {
    getType: getType,
    VehicleTypes: ordered
};
