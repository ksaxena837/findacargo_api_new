var util = require('util');

function NoVehicleAvailableError() {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = "No vehicle Available";
    this.code = 101;
}

util.inherits(NoVehicleAvailableError, Error);
module.exports = NoVehicleAvailableError;
