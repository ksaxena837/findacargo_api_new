var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Rate = new Schema({
    "userID": {
        "type": ObjectId,
        "required": true
    },
    "vehicleID": {
        "type": ObjectId,
        "required": true
    },
    "basePrice": {
        "type": Number,
        "required": true
    },
    "pricePerKm": {
        "type": Number,
        "required": true
    }
});

module.exports = mongoose.model("Rate", Rate);
