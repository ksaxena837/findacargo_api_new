var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var VehicleType= new Schema({
    "typeId": {
        "type": Number,
        "required": true
    },
    "description": {
        "type": String,
        "required": true,
    },
    "cargo": {
        "type": String
    },
    "imagePath": {
        "type": String
    },
    "base": {
        "type": Number
    },
    "costPerKm": {
        "type": Number
    },
    "velocity": {
        "type": Number
    }
});

module.exports = mongoose.model("VehicleType", VehicleType);


