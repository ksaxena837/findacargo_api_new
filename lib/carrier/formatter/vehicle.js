var _ = require("lodash");
var getType = require("../enum/vehicle-types").getType;
var Formatter = {
    availability: availability,
    bidVehicle: bidVehicle,
    updateStatus: updateStatus,
    list: list
}

function availability(typesList, pickUpLocation, dropOffLocation, distance) {
    var out = {
        "available_vehicles": formatVehiclesTypes(typesList),
        "pickup_location": formatLocation(pickUpLocation),
        "dropoff_location": formatLocation(dropOffLocation),
        "delivery_distance": formatDistance(distance),
        "units": {
            "volume": "m3",
            "area": "m2",
            "distance": "km",
            "time": "min",
            "weight": "kg",
            "currency": "kr"
        }
    };

    return out;
}

function formatVehiclesTypes(typesList) {
    var output = [];

    for(var i in typesList){
        var type = getType(typesList[i]["_id"].id);
        var item = {
            "type": {
                "id": typesList[i]["_id"].id,
                "description": type.description,
                "cargo_limit": type.cargo,
                "category": type.category
            },
            "distance": formatDistance(typesList[i].distance),
            "pickup_time": typesList[i].timeToPickUp,
            "estimated_cost": typesList[i].price,
            "vehicles": formatVehicles(typesList[i].vehicles),
            "base_price": typesList[i].basePrice,
            "price_per_km": typesList[i].pricePerKm
        };

        output.push(item)
    }

    return _.orderBy(output, ["base_price", "type.id"], ["asc", "asc"]);
}

function formatVehicles(vehicles) {
    var out = [];

    for(var i in vehicles) {
        if(vehicles[i] === null) {
            continue;
        }
        var item = {
            vehicle_id: vehicles[i].id,
            current_location: {
                lat: vehicles[i].distance.location.coordinates[1],
                long: vehicles[i].distance.location.coordinates[0],
            },
            description: vehicles[i].description,
            distance: formatDistance(vehicles[i].distance.calculated)
        };

        out.push(item);
    }

    return out;
}

function bidVehicle(vehicle) {
    return {
        "carrier_id": vehicle.userID,
        "id": vehicle._id,
        "identifier": vehicle.identifier,
        "type": vehicle.type,
    }
}

function formatLocation(locate) {
    if(typeof locate == "object"){
        return {
            "description": "",
            "coordinates": {
                "lat": locate.latitude,
                "long": locate.longitude
            }
        }
    }

    return {};
}

function formatDistance(distance) {
    if(distance >= 0){
        return Math.round(distance/10)/100;
    }

    return "N/A";
}

function updateStatus(data) {
    return {
        "message": "OK"
    }
}

function list(data) {
    return data.map(function(row){
        var keys = Object.keys(row);
        var type = getType(row.type);
        return {
            "vehicle_id": row._id,
            "type": {
                "id": row.type,
                "description": type.description,
            },
            "identifier": row.identifier,
            "online": (keys.indexOf("liveDelivery") >= 0)? row.liveDelivery.available: false,
        }
    });
}
module.exports = Formatter;
