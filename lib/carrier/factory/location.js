var factory = {
    createFromCommaSeparatedString: createFromCommaSeparatedString
};

function createFromCommaSeparatedString(string) {
    if(typeof string == "undefined"){
        return;
    }
    var coordinates = string.split(',');
    if(coordinates.length !== 2){
        throw "Invalid location";
    }

    return new Location(parseFloat(coordinates[0]), parseFloat(coordinates[1]));
}

function Location(latitude, longitude){
    this.latitude = latitude;
    this.longitude = longitude;
}

module.exports = factory;

