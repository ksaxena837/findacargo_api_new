var CarrierAssigned = require("./carrier-assigned");
var DeliveryRepository = require("../delivery/").Repository;
var CarrierStateEngine = require("../delivery/").CarrierStateEngine;
var CarrierStateEnum = require("../delivery/carrier-state/enum");

var carrierAssigned = new CarrierAssigned({
    deliveryRepository: DeliveryRepository,
    carrierStateEngine: CarrierStateEngine,
    CarrierStateEnum: CarrierStateEnum
});

module.exports = {
    CarrierAssigned: carrierAssigned
}
