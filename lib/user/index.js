var Rating = require("./rating");
var Locations = require("./locations");

var dep = {
    repository: require("./repository/account"),
    formatter: require("./formatter/account"),
    getUser: require("../auth/user").getUser
};

var rating = new Rating(dep);


var locations = new Locations({
    getUser: require("../auth/user").getUser,
    deliveryRepository: require("../delivery").Repository,
    preferredLocationsRepository: require("./repository/preferred-locations")
});

module.exports = {
    Rating: rating,
    accountRepository: require("./repository/account"),
    Locations: locations
};
