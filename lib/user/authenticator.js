var q = require("q");
var crypto = require("crypto");
var authToken = require("../framework/auth-token");
var accountRepository = require("./repository/account");
var formatter = require("./formatter/account");
var userFactory = require("./factory/user");
var getUser = require("../auth/user").getUser;
var isIOS = require("../auth/user").isIOS;
var AuthUser = require("../auth/user");
var Mixpanel = require('mixpanel/lib/mixpanel-node');
var mixpanel = Mixpanel.init('39d0bbc94e6d6a46185436ae8e5112a9');

var vehicleRepository = require('../carrier/repository/vehicle');
var vehicleSwitcher = require('../carrier').VehicleSwitcher;
var Promise = require("bluebird");

var vehicleRepository = require('../carrier/repository/vehicle');
var vehicleSwitcher = require('../carrier').VehicleSwitcher;
var Promise = require("bluebird");

var Authenticator = {
    login: login,
    register: register,
    facebookRegister: facebookRegister,
    facebookLogin: facebookLogin,
    onBoardingStepFirst: onBoardingStepFirst,
    onBoardingStepSecond: onBoardingStepSecond,
    registerDevice: registerDevice,
    logout: logout
};

function register(userData) {

    return accountRepository
        .getByEmail(userData.email)
        .then(function(account) {
            if (account) {
                throw "User/Email Id already Taken";
            }

            userData.password = encrypt(userData.password);
            userData.date = new Date();

            var newUserData = userFactory.createFromRequestUser(userData);

            return accountRepository.createAccount(newUserData);
        })
        .then(function(account) {
            mixpanel.track("Signup with Email", account);
            account._token = authToken.sign(account);
            return account;
        })
        .then(formatter.login);
}

function facebookRegister(userData) {
    return accountRepository
        .getByEmail(userData.email)
        .then(function(account) {
            if (account) {
                account.fbPassword = encrypt(userData.password);
                return accountRepository.updateAccount(account);
            }

            userData.password = encrypt(userData.password);
            userData.date = new Date();

            var newUserData = userFactory.createFromRequestFacebookUser(userData);

            return accountRepository.createAccount(newUserData);
        })
        .then(function(account) {
            mixpanel.track("Signup with Facebook", account);
            account._token = authToken.sign(account);
            return account;
        })
        .then(formatter.login);
}

function onBoardingStepFirst(data) {
    var user = getUser();



    return accountRepository.getById(user.id)
        .then(function(account) {
            if (!account) {
                throw "Invalid User";
            }

            var obsfData = userFactory.onBoardingStepFirst(data, user.id);

            return accountRepository.obsf(obsfData);
        })
        .then(formatter.onBoardingStepFirst);
}

function onBoardingStepSecond(data) {
    var user = getUser();

    return accountRepository.getById(user.id)
        .then(function(account) {

            if (!account) {
                throw "Invalid User";
            }

            var obssData = userFactory.onBoardingStepSecond(data, user.id);

            return accountRepository.obss(obssData);
        })
        .then(formatter.onBoardingStepSecond);
}

function login(user) {
    return accountRepository
        .getByEmail(user.email)
        .then(function(account) {
            if (!account) {
                throw "Invalid User";
            }

            if (account.active != 1) {
                throw "Invalid User";
            }

            var isValidUser = validatePass(user.password, account.pass);
            if (!isValidUser) {
                throw "Invalid Password";
            }

            account.platform = AuthUser.getUserAgent();
            mixpanel.track("Login with Email", account);

            account._token = authToken.sign(account);
            return account;
        })
        .then(formatter.login);
}
//set to identify users comming from different channels.
mixpanel.track(AuthUser.uid);
mixpanel.people.set({
    '$last_login': new Date()
});

if (AuthUser.facebook) {
    mixpanel.people.set({
        '$name': AuthUser.facebook.displayName
    });
    mixpanel.people.set({
        'provider_id': authUser.uid
    });
}

if (AuthUser.findacargo) {
    mixpanel.people.set({
        '$name': AuthUser.findacargo.displayName
    });
    mixpanel.people.set({
        'provider_id': AuthUser.uid
    });
}

function facebookLogin(user) {
    return accountRepository
        .getByEmail(user.email)
        .then(function(account) {
            if (!account) {
                throw "Invalid User";
            }

            if (account.active != 1) {
                throw "Invalid User";
            }

            var isValidUser = validatePass(user.password, account.fbPassword);
            if (typeof account.fbPassword != "undefined") {
                if (!isValidUser && account.fbPassword.length > 10) {
                    throw "Invalid Facebook Login";
                }
            }

            if (!isValidUser) {
                account.fbPassword = encrypt(user.password);
                return accountRepository.updateAccount(account);
            }

            return account;
        })
        .then(function(account) {
            account.platform = AuthUser.getUserAgent();

            mixpanel.track("Login with Facebook", account);

            account._token = authToken.sign(account);
            return account;
        })
        .then(formatter.login);
}

function registerDevice(data) {
    var user = getUser();
    var isIOSDevice = isIOS();

    if (user.device === data.token) {
        return formatter.login(user);
    }

    return accountRepository.updateDeviceToken(user.id, data.token, isIOSDevice)
        .then(formatter.login);
}

function generateSalt() {
    var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
    var salt = '';
    for (var i = 0; i < 10; i++) {
        var p = Math.floor(Math.random() * set.length);
        salt += set[p];
    }
    return salt;
}

function md5(str) {
    return crypto.createHash('md5').update(str).digest('hex');
}

function encrypt(pass) {
    var salt = generateSalt();
    return salt + md5(pass + salt);
}

function validatePass(plain, hashed) {
    if (typeof hashed == "undefined") {
        return false;
    }

    var salt = hashed.substr(0, 10);
    return hashed === (salt + md5(plain + salt));
}

function logout() {
    var user = getUser();

    vehicleRepository.findByCarrierId(user.id)
        .then(function(vehicles){
            var promises = vehicles.map(function(item){
                return vehicleSwitcher.turnOff(item._id);
            });

            return Promise.all(promises);
        })
}

module.exports = Authenticator;
