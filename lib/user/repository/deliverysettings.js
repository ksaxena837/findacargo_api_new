var DeliverySettings = require("../../../models/deliverysettings");

var Repository = {
    getByClientIds: getByClientIds,
    getByClientId: getByClientId
};

function getByClientIds(clientIds) {
    return DeliverySettings.find({
        'clientId': { $in: clientIds }
    });
}

function getByClientId(clientId) {
    return DeliverySettings.findOne({
        'clientId': clientId
    });
}

module.exports = Repository;
