var States = require("./enum");
var BaseState = require("./base-state");

function Rejected(Delivery, carrier, StateEngine) {
    BaseState.call(this, Delivery, carrier, StateEngine, States.Rejected);
}

Rejected.prototype = Object.create(BaseState.prototype);
Rejected.prototype.constructor = Rejected;

module.exports = Rejected;
