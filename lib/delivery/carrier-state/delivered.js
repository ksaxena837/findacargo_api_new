var States = require("./enum");
var BaseState = require("./base-state");

function Delivered(Delivery, carrier, StateEngine) {
    BaseState.call(this, Delivery, carrier, StateEngine, States.Delivered);
}

Delivered.prototype = Object.create(BaseState.prototype);
Delivered.prototype.constructor = Delivered;

module.exports = Delivered;
