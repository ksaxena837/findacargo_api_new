var States = require("./enum");
var BaseState = require("./base-state");
var AtDestination = require("./at-destination");

function OnRoute(Delivery, carrier, StateEngine) {
    BaseState.call(this, Delivery, carrier, StateEngine, States.OnRoute);

    this.setAtDestination = setAtDestination;

    function setAtDestination() {
        var state = new AtDestination(this.Delivery, this.carrier, this.StateEngine, States.AtDestination);

        return state.apply();
    }
}

OnRoute.prototype = Object.create(BaseState.prototype);
OnRoute.prototype.constructor = OnRoute;

module.exports = OnRoute;
