var States = require("./enum");
var deliveryRepository = require("../repository/delivery");

var options = {
    currentState: "Base",
    Delivery: null,
    StateEngine: {} 
}

function BaseState(Delivery, carrier, StateEngine, current){
    options.Delivery = Delivery;
    options.StateEngine = StateEngine;
    options.currentState = current;
    this.Delivery = Delivery;
    this.StateEngine = StateEngine;
    this.carrier = carrier;

    this.setInvited = setInvited;
    this.setAccepted = setAccepted;
    this.setRejected = setRejected;
    this.setArrived = setArrived;
    this.setOnRoute = setOnRoute;
    this.setAtDestination= setAtDestination;
    this.setDelivered =  setDelivered;
    this.setStarted =  setStarted;
    this.setPickedUp =  setPickedUp;
    this.state = current;
    this.apply = apply;

    function setInvited() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Carrier Invited";
    }

    function setAccepted() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Carrier Accepted";
    }

    function setRejected() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Carrier Rejected";
    }

    function setArrived() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Carrier Arrived";
    }

    function setOnRoute() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Carrier On Route";
    }

    function setAtDestination() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Carrier At Destination";
    }

    function setDelivered() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Carrier Delivered";
    }
    function setStarted() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Started";
    }
    function setPickedUp() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Picked Up";
    }

    function apply() {
        for(var n in this.Delivery.carriers) {
            if(this.Delivery.carriers[n].accountId.toString() === this.carrier.accountId.toString()){
                break;
            }

            if(this.Delivery.carriers.length === parseInt(n)+1){
                throw "Can't set the carrier information.";
            }
        }

        var log = {
            "fromStatus": this.Delivery.carriers[n].status,
            "toStatus": this.state.id
        };

        this.Delivery.carriers[n].status = this.state.id;
        if(!this.Delivery.carriers[n].progressLog) {
            this.Delivery.carriers[n].progressLog = [];
        }
        this.Delivery.carriers[n].progressLog.push(log);

        this.StateEngine.setState(this);
        return deliveryRepository.update(this.Delivery);
    }
}

module.exports = BaseState;
