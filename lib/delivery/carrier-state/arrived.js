var States = require("./enum");
var BaseState = require("./base-state");
var OnRoute = require("./on-route");

function Arrived(Delivery, carrier, StateEngine) {
    BaseState.call(this, Delivery, carrier, StateEngine, States.Arrived);

    this.setOnRoute = setOnRoute;

    function setOnRoute() {
        var state = new OnRoute(this.Delivery, this.carrier, this.StateEngine, States.OnRoute);

        return state.apply();
    }
}

Arrived.prototype = Object.create(BaseState.prototype);
Arrived.prototype.constructor = Arrived;

module.exports = Arrived;
