var States = require("./enum");
var BaseState = require("./base-state");
var Arrived = require("./arrived");

function Accepted(Delivery, carrier, StateEngine) {
    BaseState.call(this, Delivery, carrier, StateEngine, States.Accepted);

    this.setArrived = setArrived;

    function setArrived() {
        var state = new Arrived(this.Delivery, this.carrier, this.StateEngine, States.Arrived);

        return state.apply();
    }
}

Accepted.prototype = Object.create(BaseState.prototype);
Accepted.prototype.constructor = Accepted;

module.exports = Accepted;
