var PaymentPending = require("./payment-pending");
var PaymentAuthorized = require("./payment-authorized");
var CarrierPending = require("./carrier-pending");
var CarrierApproved = require("./carrier-approved");
var Canceled = require("./canceled");
var InProgress = require("./in-progress");
var Finished = require("./finished");
var Moved = require("./moved");

var States = {
    PaymentPending: PaymentPending,
    PaymentAuthorized: PaymentAuthorized,
    CarrierPending: CarrierPending,
    CarrierApproved: CarrierApproved,
    Canceled: Canceled,
    InProgress: InProgress,
    Finished: Finished,
    Moved: Moved
}

module.exports = States;
