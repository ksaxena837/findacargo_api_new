var States = require("./enum");
var BaseState = require("./base-state");
var CarrierPending = require("./carrier-pending");

function Moved(Delivery, setState) {
    BaseState.call(this, Delivery, setState, States.Moved);

    var that = this;
    this.setCarrierPending = setCarrierPending;

    function setCarrierPending() {
        var Delivery = this.Delivery;
        var setState = this.setState;

        var newState = new CarrierPending(Delivery, setState);
        setState(newState);
        return newState.apply();
    }
    function setCanceled() {
        throw "You can't cancel a moved delivery!"
    }
}

Moved.prototype = Object.create(BaseState.prototype);
Moved.prototype.constructor = Moved;

module.exports = Moved;
