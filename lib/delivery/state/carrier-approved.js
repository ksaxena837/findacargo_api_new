var States = require("./enum");
var BaseState = require("./base-state");
var InProgress = require("./in-progress");

function CarrierApproved(Delivery, setState) {
    BaseState.call(this, Delivery, setState, States.CarrierApproved);

    this.setInProgress = setInProgress;

    function setInProgress() {
        var Delivery = this.Delivery;
        var setState = this.setState;

        var newState = new InProgress(Delivery, setState);
        setState(newState);
        return newState.apply();
    }

    function setCanceled() {
        throw "You can't cancel a delivery accepted from a carrier!"
    }
}

CarrierApproved.prototype = Object.create(BaseState.prototype);
CarrierApproved.prototype.constructor = CarrierApproved;

module.exports = CarrierApproved;
