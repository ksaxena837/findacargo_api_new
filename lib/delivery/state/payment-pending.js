var States = require("./enum");
var BaseState = require("./base-state");
var PaymentAuthorized = require("./payment-authorized");

function PaymentPending(Delivery, setState, scheduler) {
    BaseState.call(this, Delivery, setState, States.PaymentPending);

    var that = this;
    this.scheduler = scheduler;
    this.setPaymentAuthorized = setPaymentAuthorized;

    function setPaymentAuthorized(){
        var Delivery = this.Delivery;
        var setState = this.setState;

        var newState = new PaymentAuthorized(Delivery, setState);
        setState(newState);
        return newState.apply()
                    .then(that.scheduler.bidVehicle);
    };

}

PaymentPending.prototype = Object.create(BaseState.prototype);
PaymentPending.prototype.constructor = PaymentPending;

module.exports = PaymentPending;
