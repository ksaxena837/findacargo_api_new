var di = {};
var mongoose = require("mongoose");
var Promise = require("bluebird");
mongoose.Promise = require("bluebird");

var ClaimsSchema = require("./repository/models/claims"),
    ClaimsModel = mongoose.models.Claims;

function Claims(depend) {
    di = depend;
    var that = this;

    this.create = create;

    function create(deliveryId, data) {
        var user = di.getUser();

        return getDelivery(deliveryId)
            .then(save)
            .then(function(data){
                return {
                    message: "Report posted successfully."
                }
            });

        function getDelivery(deliveryId) {
            return di.deliveryRepository.getById(deliveryId);
        }
        
        function save(delivery) {
            data.userId = user.id;
            data.deliveryId = delivery._id;
            var model = new ClaimsModel(data);

            return model.save();
        }
    }
}

module.exports = Claims;
