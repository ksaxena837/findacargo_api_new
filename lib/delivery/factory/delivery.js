var moment = require("moment");

var Factory = {
    createFromRequestDelivery: createFromRequestDelivery,
    createCarrier: createCarrier,
    createFromScheduled: createFromScheduled
}

var Delivery = function(description, status, buyer, pickUp, dropOff, cargo, carriers, notes, promocode, values, vehicleType, options, type, reference, groupRef, extra){
    this.description = description;
    this.status = status;
    this.buyer = buyer;
    this.pickUp = pickUp;
    this.dropOff = dropOff;
    this.cargo = cargo;
    this.carriers = carriers;
    this.notes = notes;
    this.promocode = promocode;
    this.price = values.price;
    this.distance = values.distance;
    this.priceDetails = values;
    this.vehicleType = vehicleType;
    this.options = options;
    this.deliveryType = type;
    this.reference = reference;
    this.groupReference = groupRef;
    this.extra = extra;
}

function createFromRequestDelivery(request, user, values) {
    var description = request.description;
    var status = 1; 
    var buyer = {
        accountId: user.id,
        name: user.name,
        phone: user.phone,
        email: user.email
    };

    var pickDate = (request.pickup_options.when === "now")? moment():  moment(request.pickup_options.date+" "+request.pickup_options.time, "YYYY-MM-DD hh:mm");
    var dropDate = moment();

    var pickUp = {
        description: request.pickup_location.description,
        location: [request.pickup_location.longitude, request.pickup_location.latitude],
        pickupDate: pickDate,
        when: request.pickup_options.when
    };
    var dropOff = {
        description: request.dropoff_location.description,
        location: [request.dropoff_location.longitude, request.dropoff_location.latitude],
        date: dropDate
    };
    var cargo = request.cargo_details;
    var carriers = [];
    var notes = request.notes;
    var promocode = request.promocode;
    var options = request.options;
    var vehicleType = request.vehicle_type;
    var type = 1;
    var reference = null;
    var extra = request.extra;

    return  new Delivery(description, status, buyer, pickUp, dropOff, cargo, carriers, notes, promocode, values, vehicleType, options, type, reference, null, extra);
}

function createCarrier(vehicleDetail, account){
    return {
        accountId: vehicleDetail.carrier_id,
        status: 1,
        vehicle: {
            id: vehicleDetail.id,
            identifier: vehicleDetail.identifier,
            type: vehicleDetail.type
        },
        price: vehicleDetail.price,
        payment: {},
        phone: account.phone,
        email: account.email,
        name: account.name,
    };
}

function createFromScheduled(scheduled, carrierAccount, group, vehicle) {
    var description = "Scheduled delivery";
    var status = 4;
    var buyer = {
        name: scheduled.buyer.name,
        phone: scheduled.buyer.phone,
        email: scheduled.buyer.email
    };
    var pickUp = {
        description: scheduled.pickup_location.description,
        location: [scheduled.pickup_location.longitude, scheduled.pickup_location.latitude],
        pickupDate: scheduled.pickup_date
    };
    var dropOff = {
        description: scheduled.dropoff_location.description,
        location: [scheduled.dropoff_location.longitude, scheduled.dropoff_location.latitude],
    }
    var cargo = "Packages "+scheduled.packages;
    var carriers = [{
        accountId: carrierAccount.id,
        phone: carrierAccount.phone,
        email: carrierAccount.email,
        status: 8,
        vehicle: {
            id: (vehicle)? vehicle._id:"",
            identifier: (vehicle)? vehicle.identifier:"",
            type: (vehicle)? vehicle.type:""
        },
    }]
    var notes = scheduled.notes;
    var promocode = "";
    var values = {
        price: null,
        distance: null
    };
    var vehicleType = null; 
    var options = [];
    var type = 2;
    var reference = scheduled.scheduled_id;
    var groupRef = group;

    return new Delivery(description, status, buyer, pickUp, dropOff, cargo, carriers, notes, promocode, values, vehicleType, options, type, reference, groupRef);
}

module.exports = Factory;
