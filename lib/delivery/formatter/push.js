var getDescription = require("../state/enum").getDescription;
var getCarrierStateDescription = require("../carrier-state/enum").getDescription;

var Formatter = {
    carrierInvited: carrierInvited,
    carrierAccepted: carrierAccepted,
    carrierAcceptedWithStatus: carrierAcceptedWithStatus,
    statusChanged: statusChanged,
    deliveryStatus: deliveryStatus,
}

function carrierInvited(delivery, vehicleData, vehicle, buyer) {
    return {
        _id: delivery._id,
        pickup_description: delivery.pickUp.description,
        pickup_location: delivery.pickUp.location,
        destination_description: delivery.dropOff.description,
        destination_location: delivery.dropOff.location,
        distance: delivery.distance,
        price: delivery.price,
        when: delivery.pickUp.when,
        pickup_date: delivery.pickUp.pickupDate,
        notes: delivery.notes,
        vehicle_id: vehicleData.id,
        vehicle_identifier: vehicleData.identifier,
        vehicle_driver: vehicle.driver.name, 
        vehicle_type: vehicleData.type,
        buyer_name: buyer.name,
        buyer_phone: buyer.phone,
        vehicle:{
            _id: vehicleData.id,
            identifier: vehicleData.identifier,
            driver: vehicle.driver.name, 
            type: vehicleData.type
        },
        buyer: {
            name: buyer.name,
            phone: buyer.phone
        },
        created_time: delivery.createdAt
    }
}

function carrierAcceptedWithStatus(delivery, carrier, vehicle, carrierAccount) {
    var output = carrierAccepted(delivery, carrier, vehicle, carrierAccount);
    output.status = delivery.status;
    return output;
}

function carrierAccepted(delivery, carrier, vehicle, carrierAccount) {
    var output = {
        _id: delivery._id,
        pickup_description: delivery.pickUp.description,
        pickup_location: delivery.pickUp.location,
        destination_description: delivery.dropOff.description,
        destination_location: delivery.dropOff.location,
        distance: delivery.distance,
        price: delivery.price,
        when: delivery.pickUp.when,
        pickup_date: delivery.pickUp.pickupDate,
        pickup_time: 0
    }

    if(typeof carrier != "undefined") {
        output.vehicle = {
            _id: carrier.accountId,
            identifier: carrier.vehicle.identifier,
            driver: carrierAccount.name,
            type: delivery.vehicleType,
            license_plate: vehicle.identifier,
            phone: carrierAccount.phone,
            location: (vehicle.liveDelivery != "undefined")? vehicle.liveDelivery.location.coordinates: []
        };
        output.carrier_status = {
            "id": carrier.status,
            "description": getCarrierStateDescription(carrier.status)
        };
    }else{
        output.vehicle = {};
        output.carrier_status = {};
    }

    return output;
}

function statusChanged(delivery, carrier){
    return {
        "_id": delivery._id.toString(),
        "status": {
            "id": delivery.status,
            "description": getDescription(delivery.status)
        },
        "carrier_status" : {
            "id": carrier.status,
            "description": getCarrierStateDescription(carrier.status)
        },
        "carrier": {
            "_id": carrier._id.toString(),
            "name": carrier.name,
            "phone": carrier.phone,
        }
    }
}

function deliveryStatus(delivery){
    return {
        "_id": delivery._id.toString(),
        "status": {
            "id": delivery.status,
            "description": getDescription(delivery.status)
        }
    }
}

module.exports = Formatter;
