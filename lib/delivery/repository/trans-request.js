var mongodb = require("mongodb");
var mongoose = require("mongoose");
mongoose.Promise = require("bluebird");
var deliverySchema = require("./models/trans-request");
var Slertchema = require("./models/alert");

var TransRequest = mongoose.models.TransRequest;
var Alert = mongoose.models.Alert;

var Repository = {
    create: create,
    getAlertsForTransRequest: getAlertsForTransRequest,
    getByUserId: getByUserId
}

function create(requestData) {
    var transRequest = new TransRequest(requestData);

    return transRequest.save();
}

function getAlertsForTransRequest(data) {
    var query = {};
    if(typeof data["pickupdate"] != 'undefined'){
        query["$or"] =  [ {when:{'$lte': data.pickupdate}}, {when:'Any Date'} ];
    }
    if(typeof data["destinationreadable"] != 'undefined'){
        query["location"] = { $elemMatch: { readable: data.destinationreadable} };
    }

    return Alert.find(query).exec();
}

function getByUserId(id) {
    var query = {
        "creator": new mongodb.ObjectId(id)
    };

    return TransRequest.find(query).exec();
}
module.exports = Repository;
