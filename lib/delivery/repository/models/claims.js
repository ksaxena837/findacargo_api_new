var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Claims = new Schema({
    "ClaimType": {
        "type": Number
    },
    "location": {
        "type": String,
    },
    "message": {
        "type": String
    },
    "image": {
        "type": String,
    },
    "notAtHome":{
        "type": Number
    },
    "notAnsweringCall":{
        "type": Number,
    },
    "deliveryId":{
        "type": ObjectId
    }
}, {collection: "claims"});

module.exports = mongoose.model("Claims", Claims);


