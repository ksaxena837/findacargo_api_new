var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var TransRequest = new Schema({
    "description": {
        "type": String
    },
    "pickupdate": {
        "type": Date,
        "default": Date.now
    },
    "pickuplocreadable": {
        "type": String
    },
    "pickuploc": {
        "type": [Number],
        "index": '2d'
    },
    "destinationreadable":{
        "type": String
    },
    "destination":{
        "type": [Number],
        "index": '2d'
    },
    "transtype": [String],
    "cargotype": {
        "type": String
    },
    "dimensions":{
        "volume":Number,
        "area":Number,
        "weight":Number
    },
    "creator": ObjectId,
    "name": String,
    "requestTitle": String,
    "bids ": [],
    "budget": Number,
    "freeload": Boolean,
    "expireAt": {
        "type": Date,
        "expires": '2m'
    }
}, {collection: "transrequests"});

module.exports = mongoose.model("TransRequest", TransRequest);


