var CarrierAcceptedNotification = require('../../notification').CarrierAcceptedNotification;
var StatusChangedNotification = require('../../notification').StatusChangedNotification;
var DeliveryCompleteNotification = require('../../notification').DeliveryCompleteNotification;
var DeliveryStartedNotification = require('../../notification').DeliveryStartedNotification;
var deliveryState = require('../delivery-state');
var pushFormatter = require('../formatter/push');
var accountRepository = require('../../user/repository/account');
var vehicleSwitcher = require('../../carrier').VehicleSwitcher;

var stateEngineEmitter = {}
var di = {};

function CarrierStateEvents(stateEngine, paymentCapturer, scheduler){
    stateEngineEmitter = stateEngine;
    di.paymentCapturer = paymentCapturer;
    di.scheduler = scheduler;

    stateEngine.on("delivery-accepted", notifyCarrierAccepted);
    stateEngine.on("cargo-picked-up", cargoPicked);
    stateEngine.on("cargo-delivered", capturePayment);
    stateEngine.on("cargo-delivered", finishDelivery);
    stateEngine.on("cargo-delivered", notifyDeliveryComplete);
    stateEngine.on("carrier-arrived", startDelivery);
    stateEngine.on("carrier-arrived", notifyStatusChanged);
    stateEngine.on("carrier-arrived", notifyDeliveryStarted);
    stateEngine.on("carrier-on-route", notifyStatusChanged);
    stateEngine.on("carrier-at-destination", notifyStatusChanged);
    stateEngine.on("cargo-delivered", notifyStatusChanged);
    stateEngine.on("cargo-delayed", notifyDeliveryDelayed);
}

function cargoPicked(delivery) {
    var state = deliveryState.load(delivery, di.scheduler)
    state.setInProgress();
}

function notifyDeliveryDelayed(deliveryModel) {
    try {
        //Getting relevant info from repository
        return accountRepository.getVehicleById(carrier.vehicle.id)
            .then(function(vehicleData) {
                var notification = new DeliveryDelayedNotification(deliveryModel);
                return notification.send({
                    carrier: carrier,
                    deliveryModel: deliveryModel,
                    vehicleData: vehicleData
                });
            })
    } catch(err) {
        console.error(err);
    }

}
function notifyDeliveryStarted(deliveryModel) {
  try {
    //Get relevant info from repository
    return accountRepository.getVehicleById(carrier.vehicle.id)
      .then(function(vehicleData) {
        var notification = new DeliveryStartedNotification(deliveryModel);
        return notification.send({
          carrier: carrier,
          deliveryModel: deliveryModel,
          vehicleData: vehicleData
        });
      })
  } catch(err) {
    console.error(err);
  }

}

function notifyDeliveryComplete(deliveryModel) {
  try {
    //Get relevant info from repository
    return accountRepository.getVehicleById(carrier.vehicle.id)
      .then(function(vehicleData) {
        var notification = new DeliveryCompleteNotification(deliveryModel);
        //send all the correct data to the notification
        return notification.send({
          carrier: carrier,
          deliveryModel: deliveryModel,
          vehicleData: vehicleData
        });
      })
  } catch(err) {
    console.error(err);
  }

}

function notifyCarrierAccepted(deliveryModel, carrier) {
    try{
        return accountRepository.getVehicleById(carrier.vehicle.id)
                .then(function(vehicleData){
                    var notification = new CarrierAcceptedNotification(deliveryModel);
                    return notification.send(pushFormatter.carrierAccepted(deliveryModel, carrier, vehicleData.vehicle, vehicleData.driver));
                });
    }catch(err){
        console.log(err);
    }
}

function capturePayment(delivery, carrier) {
    return di.paymentCapturer.capture(delivery)
            .catch(console.log);
}

function finishDelivery(delivery, carrier) {
    try{
        var state = deliveryState.load(delivery, di.scheduler)
        state.setFinished();
        vehicleSwitcher.turnOn(carrier.vehicle.id);
    }catch(err){
        console.log(err);
    }
}

function startDelivery(delivery, carrier) {
    try{
        var state = deliveryState.load(delivery, di.scheduler)
        state.setInProgress();
        vehicleSwitcher.turnOff(carrier.vehicle.id);
    }catch(err){
        console.log(err);
    }
}

function notifyStatusChanged(delivery, carrier) {
    try{
        var notification = new StatusChangedNotification(delivery.buyer.accountId);
        return notification.send(pushFormatter.statusChanged(delivery, carrier));
    }catch(err){
        console.log(err);
    }
}

module.exports = CarrierStateEvents;
