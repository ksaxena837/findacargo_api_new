var deliveryRepository = require("./repository/delivery");
var deliveryFormatter = require("./formatter/delivery");
var getUser = require("../auth/user").getUser;

function CarrierActions(carrierStateEngine){
    this.accept = accept;
    this.reject = reject;
    this.arrive = arrive;
    this.onRoute = onRoute;
    this.atDestination = atDestination;
    this.finish = finish;
    this.pickedUp = pickedUp;

    function accept(deliveryId) {
        return getDelivery(deliveryId)
                .then(loadState)
                .then(carrierStateEngine.setAccepted)
                .then(format);
    }

    function reject(deliveryId) {
        return getDelivery(deliveryId)
                .then(loadState)
                .then(carrierStateEngine.setRejected)
                .then(format);
    }

    function arrive(deliveryId) {
        return getDelivery(deliveryId)
                .then(loadState)
                .then(carrierStateEngine.setArrived)
                .then(format);
    }

    function onRoute(deliveryId) {
        return getDelivery(deliveryId)
                .then(loadState)
                .then(carrierStateEngine.setOnRoute)
                .then(format);
    }

    function atDestination(deliveryId) {
        return getDelivery(deliveryId)
                .then(loadState)
                .then(carrierStateEngine.setAtDestination)
                .then(format);
    }

    function finish(deliveryId) {
        return getDelivery(deliveryId)
                .then(loadState)
                .then(carrierStateEngine.setDelivered)
                .then(deliveryRepository.updateScheduled)
                .then(format);
    }

    function pickedUp(deliveryId) {
        return getDelivery(deliveryId)
                .then(loadState)
                .then(carrierStateEngine.setPickedUp)
                .then(deliveryRepository.updateGroupReference)
                .then(format);
    }

    function format(delivery) {
        var user = getUser();
        var carrier = {};

        for(var i in delivery.carriers) {
            if(user.id.toString() == delivery.carriers[i].accountId.toString()){
                carrier = delivery.carriers[i];
                break;
            }
        }

        return deliveryFormatter.deliveryAndCarrierStatus(delivery, carrier);
    }

    function getDelivery(deliveryId) {
        var user = getUser();

        return deliveryRepository
                .getByIdAndCarrier(deliveryId, user.id);
    }

    function loadState(delivery) {
        var user = getUser();
        return carrierStateEngine.load(delivery, user.id);
    }

};

module.exports = CarrierActions;
