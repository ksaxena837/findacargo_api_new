var DeliveryState = require("./delivery-state");
var deliveryRepository = require("./repository/delivery");
var formatter = require("./formatter/delivery");
var getUser = require("../auth/user").getUser;
var q = require("q");

var di = {};

function Cancelation(scheduler) {
    di.scheduler = scheduler;
    this.cancel = cancel;
}

function cancel(deliveryId) {
    var user = getUser();

    return deliveryRepository
            .getById(deliveryId, user.id)
            .then(updateState)
            .then(emmitNotification)
            .then(formatter.deliveryStatus);
}

function updateState(Delivery) {
    var state = DeliveryState.load(Delivery, di.scheduler);

    return state.setCanceled();
}

function emmitNotification(Delivery) {
    di.scheduler.emit("delivery-canceled", Delivery);
    return Delivery;
}

module.exports = Cancelation;
