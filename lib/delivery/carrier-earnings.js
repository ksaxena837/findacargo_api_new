var moment = require("moment");
var States = require("./state/enum");
var di = {};

function CarrierEarnings(depend){
    di = depend;
    var that = this;

    this.report = report;

    function report(date) {
        var data = {};

        return getDeliveriesReport(date)
            .then(di.deliveryFormatter.report);

        function getDeliveriesReport(date){
            var dateRange = [];

            if(moment(date, "YYYY-MM-DD", true).isValid()) {
                dateRange.push(moment(date, "YYYY-MM-DD", true));
                dateRange.push(moment(date, "YYYY-MM-DD", true).add(1,'days'));
            }

            var user = di.getUser();
            return di.deliveryRepository.getCarrierEarnings(user.id, [States.Finished.id], dateRange)
                .then(function(reportData){
                    var report = {
                        total: 0,
                        fees: 0,
                        count: 0,
                        date: null,
                        deliveries: []
                    };

                    if(reportData.length > 0) {
                        var report = reportData[0];
                    }

                    if(dateRange.length > 0){
                        report.date = dateRange[0].format("YYYY-MM-DD");
                    }

                    report.amount = Math.floor((report.total-report.fees)*100)/100;
                    return report;
                });
        }

    }
}

module.exports = CarrierEarnings;
