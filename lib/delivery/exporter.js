var util = require("util");
var EventEmitter = require("events").EventEmitter;
var di = {};

function Exporter(depend) {
    EventEmitter.call(this);
    di = depend;

    var that = this;

    this.toDashboard = toDashboard;
    this.transRequest = {};

    function toDashboard(delivery) {
        return createTransRequestFromDelivery(delivery)
                .then(emitExported);

        function emitExported(transRequest) {
            that.emit("delivery-exported", transRequest, delivery);
            return delivery;
        }
    }

    function createTransRequestFromDelivery(delivery) {
        var requestData = di.factory.fromDelivery(delivery);

        return di.repository.create(requestData);
    }

}

util.inherits(Exporter, EventEmitter);
module.exports = Exporter;
