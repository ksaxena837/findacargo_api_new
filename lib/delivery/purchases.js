var moment = require("moment");
var States = require("./state/enum");
var di = {};

function Purchases(depend){
    di = depend;
    var that = this;

    this.list = list;

    function list() {
        var data = {};

        return getLiveDeliveries()
            .then(di.deliveryFormatter.list);

        function getLiveDeliveries(){
            var user = di.getUser();
            return di.deliveryRepository.getBuyerPurchases(user.id, [States.Finished.id]);
        }

    }
}

module.exports = Purchases;
