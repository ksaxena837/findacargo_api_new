
var di = {};

function MonthlyInvoice(options) {
    di = options;

    var that = this;

    this.authorize = authorize;

    function authorize(deliveryId){
        var user = di.getUser();

        validateUserAllowed(user);

        return di.deliveryRepository.getById(deliveryId, user._id.toString())
                .then(function(delivery){
                    var state = di.deliveryState.load(delivery, di.scheduler);
                    return state.setPaymentAuthorized();
                })
                .then(di.deliveryFormatter.deliveryStatus);
    }

    function validateUserAllowed(user) {
        if(user.monthlyInvoice === "1"){
            return user;
        }
        
        throw "Monthly Invoice not available to user!"
    }
}

module.exports = MonthlyInvoice;
