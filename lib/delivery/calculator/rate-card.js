var getType = require('../../carrier/enum/vehicle-types').getType;
var RateCard = {
    getByType: getByType
};

function getByType(type) {
/*    var rateCard = {
        1: {base: 90, costPerKm: 10 , extra: 10},
        2: {base: 90, costPerKm: 10 , extra: 10},
        3: {base: 150, costPerKm: 10 , extra: 10},
        4: {base: 250, costPerKm: 10 , extra: 10},
        5: {base: 250, costPerKm: 10 , extra: 10},
        6: {base: 250, costPerKm: 10 , extra: 10},
        7: {base: 250, costPerKm: 10 , extra: 10},
        8: {base: 250, costPerKm: 10 , extra: 10},
        9: {base: 250, costPerKm: 10 , extra: 10},
        10: {base: 250, costPerKm: 10 , extra: 10},
        11: {base: 250, costPerKm: 10 , extra: 10},
        12: {base: 250, costPerKm: 10 , extra: 10},
        13: {base: 250, costPerKm: 10 , extra: 10},
    };

    return rateCard[type];
*/
    return getType(type);
}

module.exports = RateCard;
