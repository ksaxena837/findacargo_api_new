var Cost = require("./cost");
var Distance = require("./distance");
var GMapsDistance = require("./external/google-distance-matrix");
var Time = require("./time");
var RateCard = require("./rate-card");

var time = new Time();
var distance = new Distance();
var cost = new Cost(distance, time, RateCard, new GMapsDistance());

module.exports = {
    Cost: cost,
    Distance: distance,
    Time: time,
    GMapsDistance: new GMapsDistance()
}
