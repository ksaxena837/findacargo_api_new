var deliveryRepository = require("./repository/delivery");
var deliveryFormatter = require("./formatter/delivery");
var getUser = require("../auth/user").getUser;

var di = {};

function CarrierActions(options){
    var that = this;
    di = options;

    this.carrierRecover = carrierRecover;

    function carrierRecover(deliveryId, request) {
        var user = di.getUser();

        return di.vehicleRepository.getVehicle(request.vehicleId, user.id)
                .then(assignDelivery)
                .then(loadState)
                .then(di.carrierStateEngine.setAccepted)
                .then(format);

        function assignDelivery(vehicle){
            return di.deliveryRepository.recoverDelivery(deliveryId)
                    .then(function(delivery){
                        if(!delivery) {
                            throw "Invalid Delivery ID";
                        }

                        return di.scheduler.assignCarrierForRecovery(delivery, di.vehicleFormatter.bidVehicle(vehicle));
                    });
        }
    }

    function format(delivery) {
        var user = di.getUser();
        var carrier = {};

        for(var i in delivery.carriers) {
            if(user.id.toString() == delivery.carriers[i].accountId.toString()){
                carrier = delivery.carriers[i];
                break;
            }
        }

        return di.deliveryFormatter.deliveryAndCarrierStatus(delivery, carrier);
    }

    function loadState(delivery) {
        var user = di.getUser();
        return di.carrierStateEngine.load(delivery, user.id);
    }


};

module.exports = CarrierActions;
