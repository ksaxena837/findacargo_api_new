var util = require("util");
var EventEmitter = require("events").EventEmitter;
var CarrierStates = require("./carrier-state");

function CarrierStateEngine(scheduler) {
    EventEmitter.call(this);

    var state;
    var carrier = {};
    var that = this;

    this.scheduler = scheduler;
    this.setInvited = setInvited;
    this.setAccepted = setAccepted;
    this.setRejected = setRejected;
    this.setArrived = setArrived;
    this.setOnRoute = setOnRoute;
    this.setAtDestination = setAtDestination;
    this.setDelivered = setDelivered;
    this.setStarted = setStarted;
    this.setPickedUp = setPickedUp;
    this.setState = setState;
    this.load = load;
    this.getState = getState;
    this.getScheduler = getScheduler;

    function load(Delivery, carrierId) {
        var lookup = {};
        for(var n in Delivery.carriers) {
            lookup[Delivery.carriers[n].accountId] = Delivery.carriers[n];
        }

        carrier = lookup[carrierId];
        if(!carrier.accountId){
            throw "Carrier not assigned to this Delivery";
        }

        switch(carrier.status){
            case 1: state = new CarrierStates.Invited(Delivery, carrier, this); break;
            case 2: state = new CarrierStates.Accepted(Delivery, carrier, this); break;
            case 3: state = new CarrierStates.Rejected(Delivery, carrier, this); break;
            case 4: state = new CarrierStates.Arrived(Delivery, carrier, this); break;
            case 5: state = new CarrierStates.OnRoute(Delivery, carrier, this); break;
            case 6: state = new CarrierStates.AtDestination(Delivery, carrier, this); break;
            case 7: state = new CarrierStates.Delivered(Delivery, carrier, this); break;
            case 8: state = new CarrierStates.Started(Delivery, carrier, this); break;
            case 9: state = new CarrierStates.PickedUp(Delivery, carrier, this); break;
        }
    }

    function setState(newState) {
        state = newState;
    }

    function getState() {
        return state;
    }

    function setInvited() {
       return  state
                .setInvited()
                .then(emitInvited);
    }

    function setAccepted() {
        return state
                .setAccepted()
                .then(emitAccepted);
    }

    function setRejected() {
        return state
                .setRejected()
                .then(emitRejected);
    }

    function setArrived() {
        return state
                .setArrived()
                .then(emitArrived);
    }

    function setOnRoute() {
        return state
                .setOnRoute()
                .then(emitOnRoute);
    }

    function setAtDestination() {
        return state
                .setAtDestination()
                .then(emitAtDestination);
    }

    function setDelivered() {
        return state
                .setDelivered()
                .then(emitDelivered);
    }

    function setStarted() {
        return state
                .setStarted();
    }

    function setPickedUp() {
        return state
                .setPickedUp()
                .then(emitPickedUp)
    }

    function emitInvited(Delivery) {
        that.emit("carrier-invited", Delivery, carrier);
        return Delivery;
    }

    function emitAccepted(Delivery) {
        that.emit("delivery-accepted", Delivery, carrier);
        return Delivery;
    }

    function emitRejected(Delivery) {
        that.emit("delivery-rejected", Delivery, carrier);
        return Delivery;
    }

    function emitArrived(Delivery) {
        that.emit("carrier-arrived", Delivery, carrier);
        return Delivery;
    }

    function emitOnRoute(Delivery) {
        that.emit("carrier-on-route", Delivery, carrier);
        return Delivery;
    }

    function emitAtDestination(Delivery) {
        that.emit("carrier-at-destination", Delivery, carrier);
        return Delivery;
    }

    function emitDelivered(Delivery) {
        that.emit("cargo-delivered", Delivery, carrier);
        return Delivery;
    }

    function emitPickedUp(Delivery) {
        that.emit("cargo-picked-up", Delivery, carrier);
        return Delivery;
    }

    function getScheduler() {
        return this.scheduler;
    }
}

util.inherits(CarrierStateEngine, EventEmitter);
module.exports = CarrierStateEngine;
