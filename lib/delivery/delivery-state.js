var States = require("./state");

var DeliveryState = {
    load: load
};

var di = {}

function load(Delivery, scheduler) {
    var state;
    di.scheduler = scheduler;
    loadCurrentState(Delivery, setState, scheduler);

    function setState(newState){
        state = newState;
    }

    function setPaymentPending() {
        return state.setPaymentPending();
    }

    function setPaymentAuthorized() {
        return state.setPaymentAuthorized();
    }

    function setCarrierPending() {
        return state.setCarrierPending();
    }

    function setCarrierApproved() {
        return state.setCarrierApproved();
    }

    function setCanceled() {
        return state.setCanceled()
                .then(cancelPayment);
    }

    function cancelPayment(delivery) {
        return di.scheduler.cancelPayment(delivery);
    }

    function setInProgress() {
        return state.setInProgress();
    }

    function setFinished() {
        return state.setFinished();
    }

    function setMoved() {
        return state.setMoved();
    }

    return {
        setPaymentPending: setPaymentPending,
        setPaymentAuthorized: setPaymentAuthorized,
        setCarrierPending: setCarrierPending,
        setCarrierApproved: setCarrierApproved,
        setCanceled: setCanceled,
        setInProgress: setInProgress,
        setFinished: setFinished,
        setMoved: setMoved
    };
}

function loadCurrentState(Delivery, setState, scheduler) {
    var current;
    switch(Delivery.status) {
        case 1: current = new States.PaymentPending(Delivery, setState, scheduler); break;
        case 2: current = new States.PaymentAuthorized(Delivery, setState); break;
        case 3: current = new States.CarrierPending(Delivery, setState); break;
        case 4: current = new States.CarrierApproved(Delivery, setState); break;
        case 5: current = new States.Canceled(Delivery, setState); break;
        case 6: current = new States.InProgress(Delivery, setState); break;
        case 7: current = new States.Finished(Delivery, setState); break;
        case 8: current = new States.Moved(Delivery, setState); break;
    }

    setState(current);
}

module.exports = DeliveryState;
