var Promise = require("bluebird");
var Strategy = require("./strategy");
var Sender = require("../sender/fcm.js");
var AccountRepository = require("../../user/repository/account");

function FCMStrategy(config) {
    Strategy.call(this, config, Sender);
    var that = this;
    this.send = send;
    
    function send(data) {
        return AccountRepository.getById(that.config.channel)
                .then(sendNotification);

        function sendNotification(account) {
            if(!account) {
                return;
            }

            if(!account.deviceToken) {
                return;
            }

            return new Promise(function(resolve, reject){
                var options = {
                    priority: "high",
                    time_to_live: 60
                };

                if(account.iOSDevice) {
                    options.notification = {
                        click_action: that.config.event,
                        body: that.config.body,
                        sound: "default"
                    }
                }else{
                    data.event_name = that.config.event;
                }

                that.sender.message(account.deviceToken, data, options, function(res){
                    resolve(res);
                });
            });
        }
    }
}


FCMStrategy.prototype = Object.create(Strategy.prototype);
FCMStrategy.prototype.constructor = FCMStrategy;

module.exports = FCMStrategy;
