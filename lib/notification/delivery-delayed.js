var Notification = require("./notification");
var TwilioStrategy = require("./strategy/twilio");
var TwilioSender = require("./sender/twilio");

function DeliveryDelayedNotification(delivery) {
    Notification.call(this, delivery);
    var that = this;

    this.delivery = delivery;
    this.configure = configure;

    function configure() {
        var pushConfig = {
          from: '+151368586565',
          body: 'You delivery has delayed'
        };

        var push = new TwilioStrategy(pushConfig, TwilioSender);

        this.addStrategy(push);
    }
}

DeliveryDelayedNotification.prototype = Object.create(Notification.prototype);
DeliveryDelayedNotification.prototype.constructor = DeliveryDelayedNotification;

module.exports = DeliveryDelayedNotification;
