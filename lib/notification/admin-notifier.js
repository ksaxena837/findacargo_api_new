var Mailgun = require("mailgun").Mailgun;

var key = "key-1d1455734118e45284625cc0aa24f3b9";
var mg = new Mailgun(key);
var AdminNotifier = {
    deliveryWithoutCarrierAlert: deliveryWithoutCarrierAlert,
    deliveryScheduledAlert: deliveryScheduledAlert,
};

var config = {
    sender: 'no-reply@findacargo.com',
    recipients: ['anibal@findacargo.com']
};

function deliveryWithoutCarrierAlert(deliveryModel) {
    var text = "The delivery #"+deliveryModel._id+" was created without a carrier.";
    mg.sendText(config.sender, config.recipients, 'SYSTEM ALERT - Delivery created without a carrier ', text, function(err){
        console.log(err);
    });
}

function deliveryScheduledAlert(deliveryModel) {
    var text = "The delivery #"+deliveryModel._id+" was scheduled to a future date and carrier was assiged to it";
    mg.sendText(config.sender, config.recipients, 'SYSTEM ALERT - Delivery scheduled to future date', text, function(err){
        console.log(err);
    });
}

module.exports = AdminNotifier;


