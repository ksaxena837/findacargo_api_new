var Notification = require("./notification");
var MailStrategy = require("./strategy/mail");
var MailSender = require("./sender/mailgun");

function TransRequestAlertNotification(Carrier) {
    Notification.call(this, Carrier);
    var that = this;

    this.Carrier = Carrier;
    this.configure = configure;

    function configure() {
        var mailConfig = {
            sender: "noreply@findacargo.com",
            recipients: that.Carrier.email,
            subject: "Alert: Avaialble delivery is looking for a carrier",
            template: "trans-request-alert.html"
        };
        var mail = new MailStrategy(mailConfig, MailSender);
        this.addStrategy(mail);
    }
}

TransRequestAlertNotification.prototype = Object.create(Notification.prototype);
TransRequestAlertNotification.prototype.constructor = TransRequestAlertNotification;

module.exports = TransRequestAlertNotification;
