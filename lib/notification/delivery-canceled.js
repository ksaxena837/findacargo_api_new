var Notification = require("./notification");
var PushStrategy = require("./strategy/push");
var FCMStrategy = require("./strategy/fcm");
var PushSender = require("./sender/push");

function DeliveryCanceledNotification(deliveryId) {
    Notification.call(this, deliveryId);
    var that = this;

    this.deliveryId = deliveryId;
    this.configure = configure;

    function configure() {
        var pushConfig = {
            channel: that.deliveryId,
            event: "delivery_canceled",
            body: "Delivery canceled"
        };

        var push = new PushStrategy(pushConfig, PushSender);
        this.addStrategy(push);

        var fcm = new FCMStrategy(pushConfig);
            this.addStrategy(fcm);
        this.addStrategy(push);
    }
}

DeliveryCanceledNotification.prototype = Object.create(Notification.prototype);
DeliveryCanceledNotification.prototype.constructor = DeliveryCanceledNotification;

module.exports = DeliveryCanceledNotification;
