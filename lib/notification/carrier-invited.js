var Notification = require("./notification");
var PushStrategy = require("./strategy/push");
var PushSender = require("./sender/push");
var FCMStrategy = require("./strategy/fcm");

function CarrierInvitedNotification(carrier) {
    Notification.call(this, carrier);
    var that = this;

    this.carrier = carrier;
    this.configure = configure;

    function configure() {
        var pushConfig = {
            channel: that.carrier._id.toString(),

            event: "carrier_invited",
            body: "New delivery request"
  

        };

        var push = new PushStrategy(pushConfig, PushSender);
        this.addStrategy(push);

        var fcm = new FCMStrategy(pushConfig);
        this.addStrategy(fcm);
    }
}

CarrierInvitedNotification.prototype = Object.create(Notification.prototype);
CarrierInvitedNotification.prototype.constructor = CarrierInvitedNotification;

module.exports = CarrierInvitedNotification;
