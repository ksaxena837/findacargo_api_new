var Mailgun = require("mailgun").Mailgun;
var mailgunConfig = require("../../config").mailgun;
var mg = new Mailgun(mailgunConfig.key);

module.exports = mg;
