var Pusher = require("pusher");
var pusherConfig = require("../../config").pusher;

var pusher = new Pusher(pusherConfig);
pusher.port = 443;

module.exports = pusher;
