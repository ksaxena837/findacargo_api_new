var Notification = require("./notification");
var TwilioStrategy = require("./strategy/twilio");
var TwilioSender = require("./sender/twilio");

function DeliveryStartedNotification(delivery) {
    Notification.call(this, delivery);
    var that = this;

    this.delivery = delivery;
    this.configure = configure;

    function configure() {
        var pushConfig = {
          from: '+151368586565',
          body: 'You delivery has started'
        };

        var push = new TwilioStrategy(pushConfig, TwilioSender);

        this.addStrategy(push);
    }
}

DeliveryStartedNotification.prototype = Object.create(Notification.prototype);
DeliveryStartedNotification.prototype.constructor = DeliveryStartedNotification;

module.exports = DeliveryStartedNotification;
