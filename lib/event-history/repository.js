let EventModel = require('../../models/eventHistory');


module.exports = {
    create: (assigned_to, assigned_to_type, event, event_data) => {
        return EventModel.create({
            assigned_to, assigned_to_type, event, event_data
        });
    },
    findForIdOfType: (assigned_to, assigned_to_type) => {
        return EventModel.find({
            assigned_to, assigned_to_type
        });
    }
};