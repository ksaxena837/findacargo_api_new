var di = {};

function Reverser(depend) {
    di = depend;

    this.cancel = cancel;

    function cancel(delivery) {
        return getPayment(delivery)
                .then(requestRefundOrCancel)
                .then(function(){
                    return delivery;
                })
                .catch(function() {
                    return delivery;
                });
    }

    function getPayment(delivery) {
        var user = di.getUser();
        var validStates = [di.states.authorized.id, di.states.captured.id];

        return di.paymentRepository.getByStatusAndDeliveryId(validStates , delivery._id, user._id);
    }

    function requestRefundOrCancel(payment) {
        return di.refundOrCancelRequest.request(payment);
    }
}

module.exports = Reverser;
