var mongoose = require("mongoose");
var paymentSchema = require("./models/payment");
var q = require("q");
var paymentStatus = require("../enum/payment-status");

var Payment = mongoose.models.Payment;

var Creator = {
    create: create
};

function create(accountId, deliveryId, value, currency, encryptedData) {
    var deferred = q.defer();
    var data = factory(accountId, deliveryId, paymentStatus.created.id, value, currency, encryptedData, "");
    var paymentModel = new Payment(data);

    paymentModel.save(function(err, data){
        if(err) {
            return deferred.reject(err);
        }
        deferred.resolve(paymentModel);
    });

    return deferred.promise;
}

function factory(accountId, deliveryId, paymentStatus, value, currency, encryptedData, merchantReference) {
    return {
        "accountId": accountId,
        "deliveryId": deliveryId,
        "status": paymentStatus,
        "value": value,
        "currency": currency,
        "encryptedData": encryptedData,
        "merchantReference": merchantReference
    }
}

module.exports = Creator;
