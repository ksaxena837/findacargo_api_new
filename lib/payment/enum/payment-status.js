var PaymentStatus = {
    created: {
        id: 1,
        desc: "CREATED"
    },
    authorized: {
        id: 2,
        desc: "AUTHORIZED"
    },
    refused: {
        id: 3,
        desc: "REFUSED"
    },
    canceled: {
        id: 4,
        desc: "CANCELLED"
    },
    pending: {
        id: 5,
        desc: "PENDING"
    },
    captured: {
        id: 6,
        desc: "CAPTURED"
    },
    error: {
        id: 7,
        desc: "ERROR"
    }
}

module.exports = PaymentStatus;
