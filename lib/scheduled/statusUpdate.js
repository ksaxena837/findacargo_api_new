let ScheduledRepository = require('./repository/scheduled'),
    request = require('request'),
    StatusHelper = require('./statusHelper'),
    Responses = require('../../services/Response'),
    EventHistoryService = require('../../services/EventHistoryService');

function StatusUpdateHandler(deliveryId, toStatus) {
    let self = this;
    toStatus = StatusHelper.getIdFromName(toStatus);

    if (toStatus === -1)
        return this.response(400, "Given status doesn't exist.");

    return ScheduledRepository.getByIdFromString(deliveryId).then((delivery) => {
        if (delivery === null)
            return this.response(400, "Delivery doesn't exist.");

        if (self.isInSameState(delivery, toStatus))
            return this.response(400, "Delivery already in given status.");

        return self.updateDeliveryStatus(deliveryId, toStatus)
            .then((updatedDelivery) => {
                updatedDelivery = updatedDelivery._doc;
                if (updatedDelivery.status == 2) {
                    let end_time = updatedDelivery.estimated_delivery_time;
                    let schedule = {
                    deliverydate: updatedDelivery.deliverydate,
                    deliverywindowend: end_time ? end_time.substring(end_time.indexOf("-") + 2) : '',
                    recipientemail: updatedDelivery.recipientemail,
                    recipientname: updatedDelivery.recipientname,
                    recipientphone: updatedDelivery.recipientphone['country_dial_code']+updatedDelivery.recipientphone['phone']
                           }
                     //Notification
                    request.post('https://notify.nemlevering.dk/api/order/create', {json: schedule}, function (err, resp, bd) {
                    if (err) console.log(err)
                    console.log(bd);
                     })
                    //end
                }
                else if (updatedDelivery.status == 3)
                {
                    let end_time = updatedDelivery.estimated_delivery_time;
                    let schedule = {
                    deliverywindowend: end_time ? end_time.substring(end_time.indexOf("-") + 2) : '',
                    recipientemail: updatedDelivery.recipientemail,
                    recipientname: updatedDelivery.recipientname,
                    recipientphone: updatedDelivery.recipientphone.country_dial_code+updatedDelivery.recipientphone.phone
                           }
                     //Notification
                    request.post('https://notify.nemlevering.dk/api/order/complete', {json: schedule}, function (err, resp, bd) {
                    if (err) console.log(err)
                    console.log(bd);
                     })
                    //end
                }
                
                return {
                    delivery_id: deliveryId,
                    status: updatedDelivery.status
                }
            }).then(data => {
                EventHistoryService.statusUpdatedScheduledDelivery(deliveryId, data.status);
                return data;
            });
    });
}

StatusUpdateHandler.prototype.isInSameState = function (delivery, toStatus) {
    return delivery.status == toStatus;
};

StatusUpdateHandler.prototype.updateDeliveryStatus = function (deliveryId, toStatus) {
    return ScheduledRepository
            .updateDeliveryStatus(deliveryId, toStatus);
};

StatusUpdateHandler.prototype.response = function (code, message) {
    return Responses.codeWithMessage(code, message);
};

module.exports = ((deliveryId, toStatus) => { return new StatusUpdateHandler(deliveryId, toStatus); });