let GeoCoder = require('../../services/GeoCoder'),
    formatScheduled = require('./formatScheduled'),
    moment = require("moment"),
    ScheduledDeliveriesRepostiroy = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    Responses = require('../../services/Response'),
    UserRepository = require('../user/repository/account'),
    DepartmentsModel = require('../../models/Departments'),
    EventHistoryService = require('../../services/EventHistoryService'),
    Versioning = require('../../utils/Versioning'),
    DeliverySettingsRepository = require('../user/repository/deliverysettings.js');

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function CreateDelivery(version, deliveries, token) {
    if (!token)
        return Responses.codeWithMessage(401, "Not authorized.");

    version = Versioning.normaliseVersion(version);

    let storedDeliveries = deliveries.map(delivery => {
        return this.storeDelivery(delivery, token, version);
    });

    return Promise.all(storedDeliveries)
        .then(result => {
            if (result.length && result.length === 1)
                result = result[0];

            return {
                http_status: 201,
                http_body: result
            };
        });
}

CreateDelivery.prototype.storeDelivery = function (delivery, token, version) {
    return new Promise((resolve, reject) => {
        this.appendCreatorUser(delivery, token)
            .then(this.codePickupAddress)
            .then(this.codeDeliveryAddress)
            .then(this.appendDates)
            .then(this.adjustDate)
            .then(this.appendDepartment)
            .then(this.store)
            .then(delivery => {
                if (version === Versioning.V1)
                    return {
                        "success": true,
                        "message": "success",
                        "created_at": delivery.updated || (new Date()).toISOString(),
                        "delivery_id": delivery._id
                    };

                return formatScheduled({from: delivery});
            })
            .then(delivery => {
                return resolve(delivery);
            })
            .catch(reject);
    });
};

CreateDelivery.prototype.appendCreatorUser = function (delivery, token) {
    return UserRepository.getByApiToken(token)
        .then(result => {
            if (!result)
                throw "Token not found.";

            delivery.creator = result._id;
            return delivery;
        });
};

CreateDelivery.prototype.codePickupAddress = function (delivery) {
    if (delivery.pickup_location.latitude && delivery.pickup_location.longitude) {
        return delivery;
    }

    return GeoCoder.codeAddress(
        delivery.pickup_location.address_1,
        delivery.pickup_location.zip
    ).then(coordinates => {
        delivery.pickup_location.latitude = coordinates.latitude;
        delivery.pickup_location.longitude = coordinates.longitude;
        return delivery;
    });
};

CreateDelivery.prototype.codeDeliveryAddress = function (delivery) {
    if (delivery.delivery_location.latitude && delivery.delivery_location.longitude) {
        return delivery;
    }

    return GeoCoder.codeAddress(
        delivery.delivery_location.address_1,
        delivery.delivery_location.zip
    ).then(coordinates => {
        delivery.delivery_location.latitude = coordinates.latitude;
        delivery.delivery_location.longitude = coordinates.longitude;
        return delivery;
    })
};

// Returns ISO 8601 week number and year
Date.prototype.getISOWeek = function () {
    var jan1, week, date = new Date(this);
    date.setDate(date.getUTCDate() + 4 - (date.getUTCDay() || 7));
    jan1 = new Date(date.getUTCFullYear(), 0, 1);
    week = Math.ceil((((date - jan1) / 86400000) + 1) / 7);
    return week;
};

CreateDelivery.prototype.appendDates = function (delivery) {
    return new Promise((resolve, reject) => {
        let deliveryDate = new Date(delivery.delivery_date);

        if (!deliveryDate.isValid())
            return reject("Invalid delivery_date.");

        delivery.weekNumber = deliveryDate.getISOWeek();
        delivery.dayOfWeek = days[deliveryDate.getUTCDay()];
        delivery.delivery_date = deliveryDate;
        return resolve(delivery);
    })
};

CreateDelivery.prototype.adjustDate = function (delivery) {
    return DeliverySettingsRepository.getByClientId(delivery.creator).then(settings => {
        if (settings && settings._doc.webshipr_integration_enabled) {
            var deliveryDays = settings._doc.week_days_available;

            if (deliveryDays.includes(delivery.dayOfWeek)) {
                return delivery;
            }

            // needed in case if the nearest possible delivery day is on the next week
            var twoWeeksDays = days.concat(days);
            var followingDays = twoWeeksDays.slice(twoWeeksDays.indexOf(delivery.dayOfWeek));
            var nearestDeliveryDay = followingDays.find(x => { return deliveryDays.includes(x); })

            delivery.delivery_date.setDate(delivery.delivery_date.getDate() + followingDays.indexOf(nearestDeliveryDay));
            console.log(delivery.delivery_date);
            delivery.weekNumber = delivery.delivery_date.getISOWeek();
            delivery.dayOfWeek = days[delivery.delivery_date.getUTCDay()];
            delivery.dateChanged = true;
        }

        return delivery;
    });
};

CreateDelivery.prototype.appendDepartment = function (delivery) {
    let creator = delivery.creator,
        givenDepartment = delivery.department || 'undefined';

    return DepartmentsModel.findOne({
        companyId: creator
    }).then(Company => {
        if (!Company)
            return delivery;

        let defaultDepartment,
            chosenDepartment;


        Company.departments.map(department => {
            if (department.default)
                defaultDepartment = department;

            if (department.name === givenDepartment)
                chosenDepartment = department;
        });

        return chosenDepartment || defaultDepartment;
    })
        .then(department => {
            delivery.department = department.name;
            return delivery;
        });
};

CreateDelivery.prototype.store = function (delivery) {
    return DeliverySettingsRepository.getByClientId(delivery.creator)
        .then((deliverySettings) => {
            if (deliverySettings && deliverySettings._doc.webshipr_integration_enabled) {
                let startDate = moment(delivery.delivery_date, "MM-DD-YYYY").startOf('day');
                let endDate = moment(delivery.delivery_date, "MM-DD-YYYY").add(1, "days");
                let filter = {
                    pickupaddress: delivery.pickup_location.address_1,
                    "recipientphone.phone": delivery.recipient_phone.phone,
                    deliverydate: {
                        $gte: startDate.toDate(),
                        $lt: endDate.toDate()
                    }
                };
                console.log(filter);
                return ScheduledDeliveriesRepostiroy.scheduled.findOneByFilter(filter)
            }
            else return null;
        })
        .then(foundDelivery => {
            if (foundDelivery) {
                let deliverynumberofpackages = (foundDelivery.deliverynumberofpackages) ? foundDelivery.deliverynumberofpackages + 1 : 2;
                return ScheduledDeliveriesRepostiroy.scheduled.findOneAndUpdate(foundDelivery._id, {deliverynumberofpackages : deliverynumberofpackages})
                    .then((res) => {
                        return res.value;
                    });
            } else {
                if (!delivery.delivery_window)
                    delivery.delivery_window = {};

                if (!delivery.recipient_phone)
                    delivery.recipient_phone = {};

                if (typeof delivery.pickup_deadline !== 'string')
                    delete delivery.pickup_deadline;

                if (typeof delivery.pickup_deadline_to !== 'string')
                    delete delivery.pickup_deadline_to;

                let department = delivery.department ? delivery.department : "";
                department = department.name ? department.name : department;

                let createObject = {
                    "deliveryid": delivery.delivery_id,
                    "department": department,
                    "recipientname": delivery.recipient_name,
                    "recipientid": delivery.recipient_id,
                    "recipientemail": delivery.recipient_email,
                    "recipientphone": delivery.recipient_phone,
                    "pickupdescription": delivery.pickup_location.description,
                    "pickupaddress": delivery.pickup_location.address_1,
                    "pickupaddress2": delivery.pickup_location.address_2,
                    "pickupzip": delivery.pickup_location.zip,
                    "pickupcity": delivery.pickup_location.city,
                    "pickupdeadline": delivery.pickup_deadline,
                    "pickupdeadlineto": delivery.pickup_deadline_to,
                    "delilverydescription": delivery.delivery_location.description,
                    "deliveryaddress": delivery.delivery_location.address_1,
                    "deliveryaddress2": delivery.delivery_location.address_2,
                    "deliveryzip": delivery.delivery_location.zip,
                    "deliverycity": delivery.delivery_location.city,
                    "deliverydayofweek": delivery.dayOfWeek,
                    "deliverywindowstart": delivery.delivery_window.start,
                    "deliverywindowend": delivery.delivery_window.end,
                    "deliverylabel": delivery.delivery_label,
                    "deliverynumberofpackages": delivery.delivery_number_of_packages,
                    "deliverynotes": delivery.delivery_notes,
                    "deliverydate": delivery.delivery_date,
                    "driveremail": delivery.driver_email,
                    "full_insurance": delivery.full_insurance,
                    "full_insurance_value": delivery.full_insurance_value,
                    "weekNumber": delivery.weekNumber,
                    "status": 1,
                    "deliverycoordinates": [
                        parseFloat(delivery.delivery_location.longitude),
                        parseFloat(delivery.delivery_location.latitude)
                    ],
                    "pickupcoordinates": [
                        parseFloat(delivery.pickup_location.longitude),
                        parseFloat(delivery.pickup_location.latitude)
                    ],
                    "creator": delivery.creator
                };
              
                return new Promise((resolve, reject) => {
                    ScheduledDeliveriesRepostiroy.scheduled.create(createObject)
                        .then(result => {
                            EventHistoryService.statusUpdatedScheduledDelivery(result._id, 1);
                            if (delivery.dateChanged) {
                                var deliveryDate = result.deliverydate;
                                var eventMessage = `Delivery moved to the next delivery day (${result.deliverydayofweek}, ${deliveryDate.getUTCMonth() + 1}/${deliveryDate.getUTCDate()})`;
                                EventHistoryService.dateChangedScheduledDelivery(result._id, eventMessage);
                            } return resolve(result);
                        })
                        .catch(reject);
                });
            }
        });
};

module.exports = ((ver, body, token) => {
    return new CreateDelivery(ver, body, token);
});
