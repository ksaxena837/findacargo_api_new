var db = require('../framework/db-connector');
let scheduledDeliveries = db.collection('scheduleddeliveries');
let clientProfile = db.collection('accounts');

function ForPrintResponse(id) {
    return new Promise((resolve, reject) => {
        scheduledDeliveries.findOne({'_id': ensureObjectId(id)}, function (err, doc) {
            if (err) return reject(err);

            let del = {};

            if (doc) {
                del.dest_name = doc.recipientname;
                del.dest_addr1 = doc.deliveryaddress;
                del.dest_addr2 = doc.deliveryaddress2;
                del.dest_zip = doc.deliveryzip;
                del.dest_city = doc.deliverycity;
                del.dest_phone = getPhoneNumber(doc.recipientphone);
                del.delivery_id = doc.deliveryid;
                del.delivery_notes = doc.deliverynotes;
                del.delivery_number_of_packages = doc.deliverynumberofpackages;
                del.client_id = doc.recipientid;
                del.order_index = doc.orderIndex;
                del.status = doc.status;
            }

            clientProfile.findOne({'_id': doc.creator}, function (e, profile) {
                if (e) return reject(e);

                if (profile) {
                    del.client_name = profile.name;
                    del.client_emailid = profile.supportEmail;
                    del.client_phone = profile.supportPhone;
                    del.client_addr = profile.companyDetails ? profile.companyDetails.companyAddress : '';
                    del.client_zip = profile.zip;
                    del.client_city = profile.city;
                }

                return resolve(del);
            })
        })
    });
}

function getPhoneNumber(phone) {
    if (!phone || (typeof phone === 'string'))
        return phone;

    let dialCode = phone.country_dial_code || "45";

    return  `(${dialCode}) ${phone.phone}`;
}

module.exports = ((id) => { return new ForPrintResponse(id); });