let ScheduledDeliveriesRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    DriverDeliveryRepository = require('../carrier-deliveries/repository/driverDeliveryRepository'),
    moment = require("moment"),
    getUser = require("../auth/user").getUser,
    _ = require('lodash'),
    formatScheduled = require('./formatScheduled');

function SearchHandler(body) {
    return this.getFilteredDeliveries(body);
}

SearchHandler.prototype.getFilteredDeliveries = function (body) {
    let scheduledDeliveries = this.getScheduledFiltered(body);
    let zoneDeliveries = this.getZoneDeliveries(body);

    return Promise.all([scheduledDeliveries, zoneDeliveries])
        .then((deliveriesList) => {
           return _.unionBy(deliveriesList[0], deliveriesList[1], '_id');
        })
        .then((deliveryList) => {
            return _.orderBy(deliveryList, ['deliverydate', 'desc']);
        })
        .then((deliveriesList) => {
            return deliveriesList.map(delivery => {
                return formatScheduled({ from: delivery });
            });
        });
};

SearchHandler.prototype.getScheduledFiltered = function (body) {
    let user = getUser(),
        query = this.formatQuery(body);

    return new Promise((resolve, reject) => {
        ScheduledDeliveriesRepository.scheduled
            .all(user.id, query)
            .sort({ deliverydate: -1 })
            .toArray((err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
    });

};

SearchHandler.prototype.getZoneDeliveries = function(body) {
    let user = getUser(),
        query = this.formatQuery(body);

    return new Promise((resolve, reject) => {
        DriverDeliveryRepository.getAssignedDeliveries(user._id.toString())
            .toArray((err, assignedDeliveries) => {
                if (err) return reject(err);

                let ids = assignedDeliveries.map(delivery => {
                    return delivery.delivery_id.toObjectId();
                });

                return ScheduledDeliveriesRepository.zone.all(ids, query)
                    .toArray((err, response) => {
                        if (err) return reject(err);
                        return resolve(response);
                    });
            });
    })
};

SearchHandler.prototype.formatQuery = function (body) {
    let date = body.date,
        status = body.status,
        keyword = body.keyword;

    let query = {};

    this.appendDate(query, date);
    this.appendStatus(query, status);
    this.appendKeywords(query, keyword);

    return query;
};

SearchHandler.prototype.appendDate = function (query, date) {
    let startDate = moment(date, "MM-DD-YYYY").startOf('day');
    let endDate = moment(date, "MM-DD-YYYY").add(1, "days");

    if (date)
        query.deliverydate = {
            $gte: startDate.toDate(),
            $lt: endDate.toDate()
        }
};

SearchHandler.prototype.appendStatus = function (query, status) {
    if (status)
        query.status = parseInt(status);
};

SearchHandler.prototype.appendKeywords = function (query, keyword) {
    if (!keyword)
        return;

    let regex = { $regex : new RegExp(keyword), $options: "i" };
    query.$or = [];
    query.$or.push({ deliveryaddress: regex });
    query.$or.push({ deliveryaddress2: regex });
    query.$or.push({ deliverydescription: regex });
    query.$or.push({ deliveryzip: regex });
    query.$or.push({ pickupaddress: regex });
    query.$or.push({ pickupaddress2: regex });
    query.$or.push({ pickupdescription: regex });
    query.$or.push({ pickupzip: regex });
    query.$or.push({ deliverycity: regex });
    query.$or.push({ pickupcity: regex });
    query.$or.push({ recipientname: regex });
};

module.exports = (body => { return new SearchHandler(body) });