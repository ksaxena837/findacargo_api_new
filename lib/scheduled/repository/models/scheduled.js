var mongoose = require('mongoose');
Schema = mongoose.Schema;
ObjectId = Schema.ObjectId;

var ProgressLog = new Schema({
    "fromStatus": {
        "type": Number,
    },
    "toStatus": {
        "type": Number,
    },
    "when": {
        "type": Date,
        "default": Date.now
    }
});

var Scheduled = new Schema({
    "delivery_id": {
        "type":String,
    },
    "deliverydate":{
        "type":Date
    },
    "status": {
        "type": Number
    },
    "carrier": {
        "accountId": {
            "type": ObjectId,
            "required": true
        },
        "status": {
            "type": Number
        },
        "vehicle": {
            "id": { "type": ObjectId},
            "identifier": { "type": String },
            "type": { "type": String }
        },
        "phone": {
            "type": String
        },
        "email": {
            "type": String
        },
        "progressLog": {
            "type": [ProgressLog]
        },
    }
}, {collection: "scheduleddeliveries"});

module.exports = mongoose.model('Scheduled', Scheduled);
