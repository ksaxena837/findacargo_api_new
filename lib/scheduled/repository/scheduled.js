let ObjectId = require("mongodb").ObjectId;
let mongoose = require("mongoose");
mongoose.Promise = require("bluebird");

let scheduledSchema = require("./models/scheduled");
let Scheduled = mongoose.models.Scheduled;
let Delivery = mongoose.models.Delivery;
let Vehicle = mongoose.models.truck;

let Repository = {
    getById: getById,
    getByIdFromString: getByIdFromString,
    bulkCreateDelivery: bulkCreateDelivery,
    getVehicle: getVehicle,
    updateDeliveryStatus: updateDeliveryStatus
};

function getById(id) {
    return Scheduled.findOne({"_id": new ObjectId(id)}).exec();
}

function getByIdFromString(id) {
    return Scheduled.findById(id);
}

function bulkCreateDelivery(deliveries) {
    let promises = deliveries.map(function(deliveryData){
        let delivery = new Delivery(deliveryData);

        return delivery.save()
                .then(updateScheduled);
    });

    return Promise.all(promises);

    function updateScheduled(delivery){
        return Scheduled.findOneAndUpdate(
            {_id: new ObjectId(delivery.reference)}, 
            {
                delivery_id: delivery._id.toString(),
                carrier: delivery.carriers[0]
            }
        );
    }
}

function getVehicle(userId){
    return Vehicle.findOne({userID: userId}).exec();
}

/**
 * Update a progress log with new status change.
 *
 * @param deliveryId
 * @param fromStatus
 * @param toStatus
 * @returns {Query}
 */
function updateDeliveryStatus(deliveryId, toStatus) {
    let query = {
        $set: {
            "status": toStatus
        }
    };
    return scheduledSchema.findByIdAndUpdate(deliveryId, query, { new: true });
}

module.exports = Repository;
