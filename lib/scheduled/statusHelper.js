const STATUSES = ['created', 'progress', 'finish'];
const DESCRIPTIVE_STATUS = ['Delivery Created', 'Delivery in progress', 'Delivered'];

function getIdFromName(name) {
    let index = STATUSES.indexOf(name);

    return index === -1 ? -1 : index + 1;
}

function getNameFromId(id) {
    return STATUSES[id];
}

function getDescription(id) {
    return DESCRIPTIVE_STATUS[id - 1];
}

module.exports = {
    getIdFromName,
    getNameFromId,
    getDescription
};