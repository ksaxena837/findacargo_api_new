let DepartmentModel = require('../../../models/Departments');

module.exports = {
    findByCompanyId: (companyId) => {
        return DepartmentModel.findOne({
            companyId
        });
    },
    findDepartmentOfCompany: (companyId, departmentName) => {
        return DepartmentModel.findOne({
            companyId: ensureObjectId(companyId)
        }).then(Company => {
            let wantedDepartment = false;
            Company.departments.map(department => {
                if (department.name === departmentName)
                    wantedDepartment = department;
            });

            return wantedDepartment;
        })
    }
};