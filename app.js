var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var https = require('https');
var fs = require('fs');

require('./services/Extender');
require('./services/EventHistoryService');

var routes = require('./routes/index');
var users = require('./routes/users');
var auth = require('./routes/auth');
var carriers = require('./routes/carriers');
var deliveries = require('./routes/deliveries');
var scheduleds = require('./routes/scheduleds');
var todaysDeliveries = require('./routes/carrier_delivery');
var departments = require('./routes/department');
var types = require('./routes/types');
var cargoStatus = require('./routes/deliveryStatus');
var admin = require('./routes/admin');
var scheduledDeliveries = require('./routes/plan');
var settings = require('./routes/settings');
var notifcationSettings = require('./routes/notificationSettings');
var zoneDelivery = require('./routes/zone_delivery');
var routePlanning = require('./routes/routePlanning');
var apiKeys = require('./routes/apiKeys');

var Auth = require('./middlewares/Auth');

var taskIdleVehicles = require('./lib/tasks/turnoff-idle-vehicles');
taskIdleVehicles();

var sendNotification = require('./routes/sendNotification');

var app = express();
const options = {
    key: fs.readFileSync('./key/findacargo.com.key'),
    cert: fs.readFileSync('./key/a2a576fc7f5f6e34.crt')
};

var cors = require('cors');
var corsOptions = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    "preflightContinue": true
};
app.use(cors(corsOptions));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/v1/users', users);
app.use('/v1/auth', auth);
app.use('/v1/carriers', carriers);
app.use('/v1/deliveries', deliveries);
app.use('/v1/scheduleds', scheduleds);
app.use('/v1/todayDelivery', todaysDeliveries);
app.use('/v1/departments', departments);
app.use('/v1/types', types);
app.use('/v1/setCargoStatus', cargoStatus);
app.use('/v1/sendNotification', sendNotification);
app.use('/v1/admin', admin);
app.use('/', scheduledDeliveries);
app.use('/v1/settings', Auth.isAuthenticated, settings);
app.use('/v1/user/notifications/settings', Auth.isAuthenticated, notifcationSettings);
app.use('/v1/zone-delivery', zoneDelivery);
app.use('/v1/route-planning', routePlanning);
app.use('/v1/keys', apiKeys);

// catch 404 and forward to error handler
/*
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
*/

// error handlers

// development error handler
// will print stacktrac

/*
if (app.get('env') === 'dev') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err,
    });
  });
}
*/

// production error handler
// no stacktraces leaked to user

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: err,
    });
});

// http.createServer(app).listen(8000, function() {
//     console.log('Express server listening on port ' + 8000);
// });

http.createServer(app).listen(3111, function() {
    console.log('Express server listening on port ' + 3111);
});

module.exports = app;