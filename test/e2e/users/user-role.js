var needle = require("needle");
var chai = require("chai");
var Promise = require("bluebird");
var MongoDB = require('mongodb').Db;
var Server = require('mongodb').Server;
var expect = chai.expect;
var assert = chai.assert;

var db = new MongoDB("findacargo_test", new Server("api.findacargo.dev", 27017, {auto_reconnect: true}), {w: 1});
db.open(function(e, d){
    if (e) {
        console.log(e);
    }
});

describe("Avoid no role user", function(){
    var request = {
        name: "Test User",
        email: "testuser@email.com",
        countryDialCode: "",
        countryISOCode: "",
        phone: "123123123",
        password: "teste123",
        date: "2016-10-10 10:00:00" 
    };

    var url = "http://api.findacargo.dev:3010/v1"
    var token = {};

    it("Using common endpoint", function(){
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/register", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(505);
                resolve(resp.body);
            });
        });
    });

    it("Using facebook endpoint", function(){
        request.password = 123460131371029758;
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/facebookRegister", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(505);
                resolve(resp.body);
            });
        });
    });

    after(function(){
        db.collection("accounts").remove({});
    });
});

describe("Carrier user", function(){
    var request = {
        name: "Test User",
        email: "testuser@email.com",
        countryDialCode: "",
        countryISOCode: "",
        carrier: "1",
        phone: "123123123",
        password: "teste123",
        date: "2016-10-10 10:00:00" 
    };

    var url = "http://api.findacargo.dev:3010/v1"
    var token = {};

    it("Using common endpoint", function(){
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/register", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                resolve(resp.body);
            });
        });
    });

    it("Login returns role", function(){
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/login", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                expect(resp.body.carrier).to.equal("1");
                expect(resp.body.buyer).to.equal("0");
                resolve(resp.body);
            })
        });
    });
    after(function(){
        db.collection("accounts").remove({});
    });
});

describe("Buyer user", function(){
    var request = {
        name: "Test User",
        email: "testuser@email.com",
        countryDialCode: "",
        countryISOCode: "",
        buyer: "1",
        phone: "123123123",
        password: "teste123",
        date: "2016-10-10 10:00:00" 
    };

    var url = "http://api.findacargo.dev:3010/v1"
    var token = {};

    it("Using facebook endpoint", function(){
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/facebookRegister", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                resolve(resp.body);
            });
        });
    });

    it("Login returns role", function(){
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/login", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                expect(resp.body.carrier).to.equal("0");
                expect(resp.body.buyer).to.equal("1");
                resolve(resp.body);
            })
        });
    });

    after(function(){
        db.collection("accounts").remove({});
    });
});

describe("Buyer+Carrier user", function(){
    var request = {
        name: "Test User",
        email: "testuser@email.com",
        countryDialCode: "",
        countryISOCode: "",
        buyer: "1",
        carrier:"1",
        phone: "123123123",
        password: "teste123",
        date: "2016-10-10 10:00:00" 
    };

    var url = "http://api.findacargo.dev:3010/v1"
    var token = {};

    it("Using common endpoint", function(){
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/register", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                resolve(resp.body);
            });
        });
    });

    it("Login returns role", function(){
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/login", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                expect(resp.body.carrier).to.equal("1");
                expect(resp.body.buyer).to.equal("1");
                resolve(resp.body);
            })
        });
    });

    after(function(){
        db.collection("accounts").remove({});
    });
});
